#! /usr/bin/env python
'''
@jstober
'''

import rospy
from gazebo_msgs.msg import LinkState
from gazebo_msgs.srv import GetLinkState
import threading
import time
import math
import argparse
import tf


from dynamic_reconfigure.server import Server
from gazebo_ros_vizmc.cfg import VizmcConfig
from gazebo_ros_vizmc.msg import MCState
from gazebo_ros_vizmc.srv import MCSetAction, MCSetActionResponse, MCReset, MCResetResponse, MCToggle
from gazebo_ros_vizmc.srv import MCRunService, MCRunServiceResponse, MCStateService, MCStateServiceResponse
from gazebo_ros_vizmc.srv import MCSetVergence, MCSetVergenceResponse, MCSetPosition, MCSetPositionResponse
import random

class TopicController(threading.Thread):

    def __init__(self):

        super(TopicController, self).__init__()

        rospy.init_node('topic_controller')
        # TODO make sure these don't disrupt the fake class
        self.pub = rospy.Publisher('/gazebo/set_link_state', LinkState)

        if hasattr(self, 'fake'):
            self.get_link_state = None
        else:
            # for checking the link state
            self.get_link_state = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)

        self.top_slide = LinkState()  # slider
        self.top_mount = LinkState()  # top_mount
        self.bottom_slide = LinkState()
        self.bottom_mount = LinkState()

        self.exit_event = threading.Event()
        self.state_lock = threading.Lock()

        # get initial link state
        if hasattr(self, 'fake'):
            pass
        else:
            self.top_slide, self.top_mount, self.bottom_slide, self.bottom_mount = self.read_link_state()

        # our update rate
        self.rate = rospy.Rate(100)

    def read_link_state(self):
        top_slider = self.get_link_state(link_name='top_slider', reference_frame='base_link').link_state
        top_mount = self.get_link_state(link_name='top_mount', reference_frame='top_slider').link_state
        bottom_slider = self.get_link_state(link_name='bottom_slider', reference_frame='base_link').link_state
        bottom_mount = self.get_link_state(link_name='bottom_mount', reference_frame='bottom_slider').link_state
        return top_slider, top_mount, bottom_slider, bottom_mount

    def run(self):
        while not self.exit_event.is_set():
            with self.state_lock:
                self.pub.publish(self.top_slide)
                self.pub.publish(self.bottom_slide)
                # rospy.loginfo('Publishing LinkState: {}'.format(self.top_mount))
                self.pub.publish(self.top_mount)
                self.pub.publish(self.bottom_mount)
            try:
                self.rate.sleep()
            except rospy.ROSInterruptException:
                break # shutting down

    def eval_tolerance(self, tolerance):
        top_slide, top_mount, bottom_slide, bottom_mount = self.read_link_state()
        result = math.fabs(top_slide.pose.position.x - self.top_slide.pose.position.x) > tolerance
        result = result or (math.fabs(bottom_slide.pose.position.x - self.bottom_slide.pose.position.x) > tolerance)
        result = result or (math.fabs(top_mount.pose.orientation.z - self.top_mount.pose.orientation.z) > tolerance)
        result = result or (math.fabs(top_mount.pose.orientation.w - self.top_mount.pose.orientation.w) > tolerance)
        result = result or (math.fabs(bottom_mount.pose.orientation.z - self.bottom_mount.pose.orientation.z) > tolerance)
        result = result or (math.fabs(bottom_mount.pose.orientation.w - self.bottom_mount.pose.orientation.w) > tolerance)

        return result # if any are true result is true


    def wait_on_position(self, tolerance=1e-3, sleep_time=1):
        """
        wait until the position is properly updated

        :param tolerance:
        :param sleep_time:
        :return:
        """

        while self.eval_tolerance(tolerance): # calls read_link_state
            rospy.loginfo('waiting for position to adjust')
            time.sleep(sleep_time)

        rospy.loginfo('position set to within tolerance')

    def update_mount_angle(self, gamma):
        ql = tf.transformations.quaternion_from_euler(0, gamma, 0 )
        qr = tf.transformations.quaternion_from_euler(0, -gamma, 0)

        with self.state_lock:
            # rospy.logerr('{}: {} {} {} {}'.format(gamma, ql[0], ql[1], ql[2], ql[3]))

            self.top_mount.pose.orientation.x = ql[0]
            self.top_mount.pose.orientation.y = ql[1]
            self.top_mount.pose.orientation.z = ql[2]
            self.top_mount.pose.orientation.w = ql[3]
            self.bottom_mount.pose.orientation.x = qr[0]
            self.bottom_mount.pose.orientation.y = qr[1]
            self.bottom_mount.pose.orientation.z = qr[2]
            self.bottom_mount.pose.orientation.w = qr[3]

    def update_slider_position(self, pos):
        with self.state_lock:
            self.top_slide.pose.position.x = pos
            self.bottom_slide.pose.position.x = pos

class FakeTopicController(TopicController):
    """ Does not actually publish or read from Gazebo. """

    def __init__(self):
        self.fake = True
        super(FakeTopicController, self).__init__()

    def run(self):
        while not self.exit_event.is_set():
            try:
                self.rate.sleep()
            except rospy.ROSInterruptException:
                break # shutting down

    def wait_on_position(self, tolerance=1e-3, sleep_time=1):
        ''' wait until the position is properly updated '''
        rospy.loginfo('position set to within tolerance')

class MountainCarController(threading.Thread):

    def __init__(self, position_controller):

        super(MountainCarController, self).__init__()

        self.position_controller = position_controller
        self.cfg_lock = threading.Lock()
        self.exit_event = threading.Event()
        self.unpause_event = threading.Event()
        self.unpause_event.set()
        self.nsteps_action = 0
        self.mc_dynamics = True

        # dynmic parameters
        self.reset(None)

        self.hard_position_limits = (-2.0, 2.0)

        self.rate = rospy.Rate(10)
        self.config_srv = Server(VizmcConfig, self.callback)
        self.mc_publisher = rospy.Publisher('/vizmc/mcstate', MCState)

        rospy.Service('/vizmc/toggle_mc', MCToggle, self.toggle_mc)
        rospy.Service('/vizmc/set_action', MCSetAction, self.set_action)
        rospy.Service('/vizmc/reset', MCReset, self.reset)
        rospy.Service('/vizmc/run_service', MCRunService, self.run_service) # used for fast mode
        rospy.Service('/vizmc/get_mcstate', MCStateService, self.get_mcstate) # used for fast mode
        rospy.Service('/vizmc/set_vergence', MCSetVergence, self.set_vergence)
        rospy.Service('/vizmc/set_position', MCSetPosition, self.set_position)

    def toggle_mc(self, request):
        self.mc_dynamics = request.toggle
        return self.mc_dynamics

    def get_mcstate(self, request):
        """
        A service version of the state information (useful for fastmode).
        """
        response = MCStateServiceResponse()
        with self.cfg_lock:
            response.position = self.position
            response.velocity = self.velocity
            response.action = self.action

        return response

    # TODO: make this the main interface to mountain car
    def run_service(self, req):
        """
        Run mountain car for the requested number of steps as a service. Do not run this if running mountain car as a thread.
        """

        with self.cfg_lock:
            self.action = req.action

            for i in range(req.nsteps):
                self.update_dynamics()

            response = MCRunServiceResponse() # next obs
            response.position = self.position
            response.velocity = self.velocity

        return response

    def callback(self, config, level):
        # this resets all the parameters - so we don't have the option to just toggle the action here

        with self.cfg_lock:
            self.freq = config['freq']
            self.power = config['power']
            self.ratio = config['ratio']
            self.shift = config['shift']
            self.action = config['action']
            self.velocity = config['velocity']
            self.position = config['position']
            self.gamma = config['gamma']

            self.position_controller.update_slider_position(self.position)
            self.position_controller.update_mount_angle(self.gamma)
            self.position_controller.wait_on_position() # wait for the position to update on the slider

        rospy.loginfo("""Reconfigure Request: {freq}, {power}, {ratio}, {shift}, {action}, {velocity}, {position}""".format(**config))
        return config

    def reset(self, req):
        with self.cfg_lock:
            self.freq = VizmcConfig.defaults['freq']
            self.power = VizmcConfig.defaults['power']
            self.ratio = VizmcConfig.defaults['ratio']
            self.shift = VizmcConfig.defaults['shift']
            self.action = VizmcConfig.defaults['action']
            self.gamma = VizmcConfig.defaults['gamma']

            if req is None:
                self.velocity = VizmcConfig.defaults['velocity']
                self.position = VizmcConfig.defaults['position']
            elif not req.random:
                self.velocity = VizmcConfig.defaults['velocity']
                self.position = VizmcConfig.defaults['position']
            else: # req.random == true
                self.position = random.uniform(-2.0, 2.0)
                self.velocity = random.uniform(-0.07,0.07)

        rospy.loginfo('Reset Request: {freq}, {power}, {ratio}, {shift}, {action}, {velocity}, {position}'.format(**VizmcConfig.defaults))
        return MCResetResponse()

    def pause(self):
        self.unpause_event.clear()

    def unpause(self):
        self.unpause_event.set()

    def set_vergence(self,req):
        response = MCSetVergenceResponse()
        response.result = True
        wait = req.wait

        with self.cfg_lock:
            self.gamma = req.gamma

        if wait:
            self.position_controller.wait_on_position()

        return response

    def set_position(self, req):
        response = MCSetPositionResponse()
        response.result = True
        wait = req.wait

        with self.cfg_lock:
            self.position = req.beta

        if wait:
            self.position_controller.wait_on_position()

        return response

    def set_action(self, req):
        assert req.action in (-1,0,1)
        response = MCSetActionResponse()


        with self.cfg_lock:
            self.action = req.action
            response.nsteps = self.nsteps_action
            self.nsteps_action = 0

        return response

    def update_dynamics(self):
        """
        The code that actually updates the state space.
        """
        # rospy.logerr('Updating dynamics: {}'.format(self.mc_dynamics))
        if self.mc_dynamics == False:
            return # don't run mountaincar

        self.velocity = self.velocity + self.action * self.power + math.cos(self.freq * self.position + self.shift) * self.ratio

        self.position = self.position + self.velocity

        if self.position >= self.hard_position_limits[1]:
            self.position = self.hard_position_limits[1]
            self.velocity = 0.0 # reset velocity on hard stop

        if self.position <= self.hard_position_limits[0]:
            self.position = self.hard_position_limits[0]
            self.velocity = 0.0

    def run(self):
        self.position_controller.update_slider_position(self.position)

        while not self.exit_event.is_set():
            self.unpause_event.wait()

            with self.cfg_lock:
                self.update_dynamics()

                self.mc_publisher.publish(position=self.position, velocity=self.velocity, action=self.action)
                self.nsteps_action += 1

            self.position_controller.update_slider_position(self.position)
            self.position_controller.update_mount_angle(self.gamma)

            try:
                self.rate.sleep()
            except rospy.ROSInterruptException:
                break # shutting down


def main():

    parser = argparse.ArgumentParser(description='Arguments to vizmc controller.')
    parser.add_argument('--no-gazebo', action='store_true', dest='isfake', default=False, help='Do not publish to Gazebo.')
    parser.add_argument('--fastmode', action='store_true', help='Enable fast mode for faster training when not connected to Gazebo.')
    args, unknown = parser.parse_known_args()

    if args.isfake:
        vizmc = FakeTopicController() # doesn't do anything
    else:
        vizmc = TopicController() # sends commands to gazebo simulator

    vizmc.start()

    mc = MountainCarController(vizmc)

    if args.fastmode:
        pass
    else:
        mc.start() # start the run loop

    r = rospy.Rate(1)

    while not rospy.is_shutdown():
        try:
            r.sleep()
        except rospy.ROSInterruptException:
            break

    vizmc.exit_event.set()
    mc.exit_event.set()

    vizmc.join()
    if args.fastmode:
        pass
    else:
        mc.join()


if __name__ == '__main__':

    main()
