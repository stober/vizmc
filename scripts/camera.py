#!/usr/bin/python
"""
Author: Jeremy M. Stober
Program: camera.py
Date: Monday, April  9 2007
Description: A simple roving eye robot.
"""

import os, sys, getopt, pdb, string

import Image, ImageChops
from random import randint
from numpy import *
import filter
import threading
MPEG1VIDEO = 0x314D4950
import retina as rt
from opencv import highgui,cv

class Camera:
    """
    This camera class uses PIL for image manipulation. It returns a
    ROI corresponding the current location of the camera in the larger
    image frame. The ROI is movable via motion commands.
    """

    def __init__(self, imfile, roi = 80, x = 0, y = 0):
        if isinstance(imfile,str):
            self.imfile = imfile
            self.im = Image.open(open(imfile))
        elif isinstance(imfile,Image.Image):
            self.im = imfile
            self.imfile = None

        self.roi = roi
        self.bbox = array([x, y, x + self.roi, y + self.roi])
        self.ul = array([self.im.size[0] / 2  - self.roi / 2, self.im.size[1] / 2 - self.roi / 2])
        self.stall = False
        self.__flip = False

    def flip(self):
        print "FLIPPING!"
        self.__flip = True

    def move(self, command):

        if self.__flip:
            command[1] = -command[1]

        if self.checkbounds(command):
            self.bbox = self.bbox + array([command[0], command[1], command[0], command[1]],dtype=int)
            self.stall = False

            if self.__flip:
                command[1] = -command[1]

            return command
        else:
            self.stall = True

            original = array(self.bbox[0:2],dtype=double)

            x = int(self.bbox[0] + command[0])
            y = int(self.bbox[1] + command[1])

            if x < 0:
                x = 0
            elif x >= self.im.size[0] - self.roi:
                x = self.im.size[0] - self.roi - 1

            if y < 0:
                y = 0
            elif y >= self.im.size[1] - self.roi:
                y = self.im.size[0] - self.roi - 1

            self.bbox = [x, y, x + self.roi, y + self.roi]

            # The actual command.
            command = array(self.bbox[0:2],dtype=double) - original

            if self.__flip:
                command[1] = -command[1]

            return command


    def checkbounds(self, command):
        """
        If false camera is out of bounds.  Used to make sure self.x
        and self.y are at least size pixels away from image border.
        """
        return (self.bbox[0] + command[0] >= 0) and \
            (self.bbox[1] + command[1] >= 0) and \
            (self.bbox[2] + command[0] < self.im.size[0]) and \
            (self.bbox[3] + command[1] < self.im.size[1])

    # The default converts the image into a numpy array.
    def snap(self,filter=filter.image2numpy):
        """
        Return filtered numpy array of camera data.
        """
        snap = self.im.crop(self.bbox)
        if filter:
            return filter(snap)


    def csnap(self,filter=None):
        return self.im.crop(self.bbox)

    def error(self):
        act = array([self.bbox[0],self.bbox[1]])
        return sqrt(sum((self.ul - act)**2))

    def random(self):
        """
        Randomly reposition the camerabox.
        """
        x = randint(0,self.im.size[0] - self.roi - 1)
        y = randint(0,self.im.size[1] - self.roi - 1)
        self.bbox = [x, y, x + self.roi, y + self.roi]

    def startrecord(self, filename):
        self.__unpauseEvent = threading.Event()
        self.__unpauseEvent.set()
        self.__stoprecordEvent = threading.Event()
        self.__stoprecordEvent.clear()

        self.recorder = threading.Thread(target=self.__record, args=(filename,))
        self.recorder.start() # start recording thread
        print "Recorder running..."

    def stoprecord(self):
        # Do we have to synchronize this assignment?
        if hasattr(self, "__stoprecordEvent"):
            self.__stoprecordEvent.set()
            self.recorder.join()
            print "Recorder stopped..."
        else:
            print "No recorder running..."



    def __record(self, filename):

        bwriter = highgui.cvCreateVideoWriter("binary."+filename,
                                              MPEG1VIDEO, 30.0, cv.cvSize(self.roi, self.roi), True)
        swriter = highgui.cvCreateVideoWriter("state."+filename,
                                              MPEG1VIDEO, 30.0, cv.cvSize(self.roi, self.roi), True)
        cwriter = highgui.cvCreateVideoWriter("color."+filename,
                                              MPEG1VIDEO, 30.0, cv.cvSize(self.roi, self.roi), True)

        retina = rt.Retina(rt.BoxLayers(range(1,5), 32, 128, rf=rt.SimpleLinearField, maxaction=0.0))

        self.iplbinary = cv.cvCreateImage(cv.cvSize(self.roi,self.roi), 8, 1)
        self.iplcolor = cv.cvCreateImage( cv.cvGetSize(self.iplbinary), 8, 3)

        #self.null = open('/dev/null','w')
        #self.stderr = os.dup(sys.stderr.fileno())

        while not self.__stoprecordEvent.isSet():

            if not self.__unpauseEvent.isSet():
                print "Paused"
                self.__unpauseEvent.wait()
                print "Unpaused"

            # Silence some annoying opencv errors.
            #os.dup2(self.null.fileno(), sys.stderr.fileno())

            img = self.snap()

            # write the color image
            cimg = self.csnap()
            filter.image2ipl(cimg, self.iplcolor)
            highgui.cvWriteFrame(cwriter, self.iplcolor)

            # write the binary image
            img = filter.smooth(img, 5)
            thresh = filter.adaptive(img, 0.99)
            img, active = filter.binary(img, thresh)
            filter.numpy2ipl(img, self.iplbinary)
            cv.cvCvtColor(self.iplbinary, self.iplcolor, cv.CV_GRAY2RGB)
            highgui.cvWriteFrame(bwriter,self.iplcolor)

            fdim = retina.fields.fdimension
            idim = retina.fields.idimension
            extents = retina.fields.extents

            retina.project(img, normalize=True)

            # write the state image
            sumstate = zeros((idim,idim))
            for (i,extent) in enumerate(extents):
                sumstate += retina.expand(i)

            maxact = len(extents)
            filter.numpy2ipl(array(sumstate / maxact * 255, dtype = 'uint8'), self.iplbinary)
            cv.cvCvtColor(self.iplbinary, self.iplcolor, cv.CV_GRAY2RGB)
            highgui.cvWriteFrame(swriter,self.iplcolor)

            #os.dup2(self.stderr, sys.stderr.fileno())



def __test():
    """
    >>> cam = Camera('images/scene.pgm')
    >>> cam.snap().shape
    (80, 80)
    """

    import doctest
    doctest.testmod()


if __name__ == "__main__":
    __test()


