#!/usr/bin/env python

__author__ = 'stober'


import numpy as np
import pylab as pl
import cPickle as pickle
import math


img = np.load('left_image.npy') #.reshape((480,640))
levels = np.load('sign_levels.npy')

left_processed = np.in1d(img, levels)
new_img = left_processed.reshape((480,640))

# project center of mass on to a one-hot horizontal array of sensors
print np.argmax(np.sum(new_img, axis=0)) # generates the features we'll use

# pl.imshow(new_img)
# pl.show()
# show


# testfile = open('../testfile4.pck')
#
# scores = []
# gammas = []
#
# gamma_dict = {}
# while True:
#     try:
#         data = pickle.load(testfile)
#     except:
#         break
#
#     gamma = data['gamma']
#     gamma = round(gamma, 2)
#     print data['gamma'], data['score']
#     if gamma_dict.has_key(gamma):
#         gamma_dict[gamma].append(data['score'])
#     else:
#         gamma_dict[gamma] = [data['score']]
#
#     scores.append(data['score'])
#     gammas.append(data['gamma'])
#
#
# #pl.plot(gammas, scores)
# #pl.show()
#
# import pprint
# pprint.pprint(gamma_dict)
#
# domain = np.arange(-0.4, 0.4, 0.1)
# sample1 = [gamma_dict[round(i,2)][0] for i in domain]
# sample2 = [gamma_dict[round(i,2)][1] for i in domain]
#
# pl.plot(domain, sample1, label='First Run')
# pl.plot(domain, sample2, label='Second Run')
#
# pl.xlabel('Vergence')
# pl.ylabel('Score')
# pl.legend(loc=3)
# pl.title('Vergence Scoring Inconsistancies')
# pl.savefig('figure1.svg')

# first = pickle.load(testfile)


# imgl = first['left_image'].reshape((480,640))
# imgr = first['right_image'].reshape((480, 640))

# pl.fig()
# pl.imshow(imgl)

# pl.fig()
# pl.imshow(imgr)

# pl.show()


# img = np.load('left_image.npy')
# # print img.shape
# # img = img.reshape((480,640))
#
#
# hist = np.histogram(img, bins=np.arange(255))
# print hist
#
# cnts, bins = hist
# sign_levels = []
#
#
# for i, cnt in zip(bins, cnts):
#     if cnt > 500:
#         img[img == i] = 0
#     elif 0 < cnt <= 500:
#         sign_levels.append(i)
#         img[img == i] = 1


# img = img.reshape((480,640))
#
# print sign_levels
# np.save('sign_levels',sign_levels)

# pl.imshow(img)
# pl.show()
#
# img = np.load('left_processed.npy')
# print img.shape
#
# img = img.reshape((480, 640))
# pl.imshow(img)
# pl.show()