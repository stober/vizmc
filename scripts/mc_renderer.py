#! /usr/bin/env python

__author__ = 'Jeremy Stober stober@gmail.com'

import rospy
from gazebo_ros_vizmc.msg import MCState
from pylab import ion, figure, draw, Rectangle, Line2D
from scipy import cos, sin
import time

class MountainCarRenderer(object):
    def __init__(self):
        rospy.init_node('simple_viz')
        self.pos = 0.0
        self.pos_vel = 0.0
        self.stopRequest = False

        # some drawing constants
        self.plotlimits = [-2.0, 2.0, -0.2, 0.2]
        self.carheight = 0.05
        self.carwidth = 0.1
        self.drawPlot()

        self.subscriber = rospy.Subscriber('/vizmc/mcstate', MCState, self.updateData)

    def updateData(self, data):
        self.pos = data.position
        self.pos_vel = data.velocity

    def drawPlot(self):
        ion()
        fig = figure(1)

        # draw car
        axes = fig.add_subplot(111, aspect='equal')
        self.box = Rectangle(xy=(self.pos - self.carwidth / 2.0, -self.carheight), width=self.carwidth, height=self.carheight)
        axes.add_artist(self.box)
        self.box.set_clip_box(axes.bbox)

        # set axes limits
        axes.set_xlim(self.plotlimits[0], self.plotlimits[1])
        axes.set_ylim(self.plotlimits[2], self.plotlimits[3])

    def step_render(self):
        self.box.set_facecolor('blue')
        self.box.set_x(self.pos - self.carwidth / 2.0)
        draw()


if __name__ == '__main__':

    render = MountainCarRenderer()

    r = rospy.Rate(100)
    while not rospy.is_shutdown():
        render.step_render()
        r.sleep()
 
