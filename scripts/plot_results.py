__author__ = 'stober'


import pylab
import numpy as np

def load_results(filename):
    fp = open(filename)
    results = np.array([[float(c) for c in i.split()] for i in fp.readlines()])
    return results

random = load_results('/Users/stober/wrk/gazebo_ros_vizmc/data/chk_pt_20141007_3/random_test_results.txt')
regular = load_results('/Users/stober/wrk/gazebo_ros_vizmc/data/chk_pt_20141007_2/results.txt')
se = load_results('/Users/stober/wrk/gazebo_ros_vizmc/data/chk_pt_20141007_4/results.txt')

pylab.plot(regular[:, 0], regular[:, 1], lw=2, label="Standard")
pylab.plot(random[:, 0], random[:, 1], lw=2, label="Random")
pylab.plot(se[:, 0], se[:, 1], lw=2, label="S.E. Features")

pylab.xlabel('# Training Steps')
pylab.ylabel('Avg. Number of Steps to Reach Goal')
pylab.title('Mountain Car Performance')
pylab.legend(loc=7)
#pylab.show()
pylab.savefig('MC_performance.png')