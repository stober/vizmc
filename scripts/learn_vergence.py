import cPickle as pickle
from pyrouette.td import TDQ
import numpy as np
import random
from pyrouette.gridworld.markovdp import MDP
from pyrouette.lspi import LSPI
import datetime


def label_actions(actions):
    new_actions = []
    for action in actions:
        if action < 0:
            new_actions.append(0)
        elif action > 0:
            new_actions.append(1)
        else:
            new_actions.append(2)
    return new_actions


def load_trace(filename):
    (scores, features, gammas) = pickle.load(open(filename))
    actions = np.array(gammas[1:]) - np.array(gammas[0:-1])
    min_score = np.min(scores)
    min_idx = (min_score == scores)

    rewards = np.zeros(len(scores))
    rewards[min_idx] = 1000

    trace = zip(features[0:-1], label_actions(actions), rewards, features[1:])
    return trace, features, min_idx


def response(c, x, variance=0.1):
    rd = np.abs(c - x)
    print rd
    return np.exp(-(variance * rd) ** 2)


def model_from_trace(trace, normalize=True):
    sparse_model = {}
    sparse_rewards = {}

    # measure transition probabilities from data
    for f1, act, r, f2 in trace:

        # setdefault returns the default value if the key doesn't exist (and adds that to the dict)
        local_model = sparse_model.setdefault(f1, {})
        next_states = local_model.setdefault(act, {})
        current_count = next_states.setdefault(f2, 0)
        sparse_model[f1][act][f2] = current_count + 1

        local_model = sparse_rewards.setdefault(f1, {})
        next_states = local_model.setdefault(act, {})
        next_states[f2] = r # rewards aren't random under the current version of pyrouette

        # if r > 0:
        #     print f1, act, r, f2

    normalized_model = sparse_model.copy() # ensures that normalized_model has the keys
    if normalize:
        for f1 in sparse_model.keys():
            local_model = sparse_model[f1]
            for act in local_model.keys():
                next_states = local_model[act]
                total_transitions = np.sum(next_states.values())
                for f2 in next_states.keys():
                    normalized_model[f1][act][f2] = float(next_states[f2]) / float(total_transitions)

    return normalized_model, sparse_rewards


def complete_model(model):
    """
    Some transistions were not included in the model - add them here.
    :param model:
    :return:
    """
    # action 2 stays put
    for f1 in model.keys():
        model[f1][2] = {f1: 1.0}

    model[185][0] = {185: 1.0}
    model[190][0] = {190: 1.0}
    model[418][1] = {418: 1.0}
    model[422][1] = {422: 1.0}

    return model


def verify_normalized_model(model):
    for f1 in model.keys():
        for act in model[f1].keys():
            assert np.sum(model[f1][act].values()) == 1.0 # probabilities must sum to one


def verify_rewards(rewards):
    for f1 in rewards.keys():
        for act in rewards[f1].keys():
            for f2 in rewards[f1][act].keys():
                if rewards[f1][act][f2] != 0:
                    return # ok!
    raise ValueError("No non-zero rewards detected!")


class SimulatedVergenceEnvironment(MDP):  # simulated but using traces from the robot

    def __init__(self, model):
        self.init_model = model
        super(SimulatedVergenceEnvironment, self).__init__(nstates=640, nactions=3)

    def initialize_model(self, a, i, j):
        try:
            return self.init_model[i][a][j]
        except KeyError:
            return 0.0

    def initialize_rewards(self, a, i, j):
        if j == 321:
            return 10.0
        else:
            return 0.0

    def phi(self, state, action):
        features = np.zeros(640 * 3 + 1)
        features[state + (640 * action)] = 1.0
        features[-1] = 1.0
        return features

    def linear_policy(self, w, ns):
        return np.argmax([np.dot(w,self.phi(ns,a)) for a in range(self.nactions)])


if __name__ == '__main__':
    today = datetime.date.today().strftime('%Y%m%d')

    if True:
        trace, features, min_idx = load_trace('data/scoring_data/trace.pck')
        policy, all = pickle.load(open('data/verg_policy_20141008.pck','r'))
        normalized_model, sparse_rewards = model_from_trace(trace)
        normalized_model = complete_model(normalized_model)
        verify_normalized_model(normalized_model)
        verify_rewards(sparse_rewards)

        sim = SimulatedVergenceEnvironment(normalized_model)
        sim.current = trace[0][0]

        for i in range(640):
            print i, sim.linear_policy(policy, i)

        # TESTING CODE HERE


    if False:
        trace, features, min_idx = load_trace('data/scoring_data/trace.pck')
        rbf_features = np.arange(0,640,10, dtype=float)

        # print rbf_features
        # print [response(c, float(features[0])) for c in rbf_features]

        normalized_model, sparse_rewards = model_from_trace(trace)
        normalized_model = complete_model(normalized_model)
        verify_normalized_model(normalized_model)
        verify_rewards(sparse_rewards)

        sim = SimulatedVergenceEnvironment(normalized_model)
        sim.current = trace[0][0]
        t = sim.trace(tlen=10000)
        tsmall = random.sample(t, k=1000)
        policy, all = LSPI(tsmall, 0.001, sim, np.zeros(sim.nfeatures()))
        pickle.dump((policy, all), open('data/verg_policy_{}.pck'.format(today),'w'), pickle.HIGHEST_PROTOCOL)

        # values, final, all_policies = sim.value_iteration(save_all=True)
        # pickle.dump((values, final, all_policies), open('data/verg_vi_{}.pck'.format(today),'w'), pickle.HIGHEST_PROTOCOL)

    if False:
        (policy, all) = pickle.load(open('verg_policy.pck'))
        print policy
        sim = SimulatedVergenceEnvironment(normalized_model)

        values, final, all_policies = sim.value_iteration(save_all=True)

        pickle.dump((values, final, all_policies), open('verg_vi.pck','w'), pickle.HIGHEST_PROTOCOL)
        print final
        # pi = [sim.linear_policy(policy, s) for s in normalized_model.keys()]
        # print pi
        # print normalized_model

    if False:
        (values, final, all_policies) = pickle.load(open('verg_vi.pck'))
        from functools import partial
        sim = SimulatedVergenceEnvironment(normalized_model)
        sim.endstates = np.array(features)[min_idx]
        start_states = normalized_model.keys()

        avg_episode_lengths = [0] * len(all_policies)
        for pcnt, policy in enumerate(all_policies):
            for state in start_states:
                elength = len(sim.single_episode(partial(sim.lookup_policy, policy), start=state))
                avg_episode_lengths[pcnt] += elength
            avg_episode_lengths[pcnt] = float(avg_episode_lengths[pcnt]) / float(len(start_states))
        print avg_episode_lengths



    if False:
        learner = TDQ(3, 640, 0.1, 0.05, 0.01, init_val = 0.0)

        # iterate through the trace data to train the value function
        nsteps = 40000
        step = 0
        while step < nsteps:
            step += 1
            if step % 1000 == 0:
                print "Steps ", step
            (pstate, paction, reward, state) = random.choice(trace)
            learner.train(pstate, paction, reward, state, random.choice([0,1,2]), delta=None)

        for (pstate, paction, reward, state) in trace:
            print pstate, 0, learner.value(0, pstate), 1, learner.value(1, pstate)