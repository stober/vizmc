__author__ = 'Jeremy Stober <stober@gmail.com>'

from pyrouette.utils import pckopen
from PIL import Image
from StringIO import StringIO
import numpy as np
import pylab
import cPickle as pickle
from fovea import SquareFovea
from pprint import pprint
from pyrouette import dtw
from pyrouette import mds

def convert_image_numpy(d, key):
    im = Image.open(StringIO(d[key]))
    return np.array(im)


def convert_image_png(d, key):
    strp = StringIO()
    im = Image.fromarray(d[key])
    im.save(strp, format='png')
    return strp.getvalue()


def process_image(img):
    h, w = img.shape
    img = img[(h / 2 - 240):(h / 2 + 240), (w / 2 - 240):(w / 2 + 240)]
    img[img < 155] = 1.0
    img[img >= 155] = 0.0
    return img


def find_minimum(pos, data):
    lowest_score = 1e6
    lowest_gamma = -1
    lowest_pos = -1
    for (gamma, pos, score) in data[pos]:
        if score < lowest_score:
            lowest_score = score
            lowest_gamma = gamma
            lowest_pos = pos
    return lowest_gamma, lowest_pos, lowest_score


def find_trace(pos, data):
    lowest_gamma, lowest_pos, lowest_score = find_minimum(pos, data)
    trace = []
    for (gamma, pos, score) in data[pos]:

        if lowest_gamma >= 0 and 0 <= gamma <= lowest_gamma:
            trace.append((gamma, pos, score))

        elif lowest_gamma < 0 and lowest_gamma <= gamma <= 0:
            trace.append((gamma, pos, score))

    return trace


def convert_pickle_file(inputfile, outputfile):
    data = pckopen(inputfile)
    fp = open(outputfile, 'w')
    for d in data:
        for key in d.keys():
            if key.endswith('_image'):
                d[key] = convert_image_png(d, key)
        pickle.dump(d, fp, pickle.HIGHEST_PROTOCOL)
    fp.close()


def convert_pickle_results(inputfile, outputfile):
    fovea = SquareFovea([1, 2, 4, 12, 24], 480, fdim=20)
    data = pckopen(inputfile)
    fp = open(outputfile, 'w')
    results = []
    for d in data:
        compare_keys = []
        for key in d.keys():
            if key.endswith('_image') and key != 'obs_image':
                d[key] = process_image(convert_image_numpy(d, key))
                compare_keys.append(key)
        assert len(compare_keys) == 2
        nscore = fovea.compare(d[compare_keys[0]], d[compare_keys[1]])
        results.append((d['gamma'], d['position'], nscore))
    pickle.dump(results, open(outputfile, 'w'), pickle.HIGHEST_PROTOCOL)


def sensorimotor_embedding_results(resultsfile):
    results = pickle.load(open(resultsfile))
    data = {}
    for gamma, pos, score in results:
        indx = np.round(pos, decimals=1)
        indx = str(indx)
        if indx in data:
            data[indx].append((gamma, pos, score))
        else:
            data[indx] = [(gamma, pos, score)]

    traces = {}
    for key in sorted(data.keys(), cmp=lambda x, y: cmp(float(x), float(y))):
        print find_minimum(key, data)
        n = len(find_trace(key, data))
        traces[key] = np.ones(n)

    dist = {}
    n = len(traces)
    mat = np.zeros((n, n))
    for i, x in enumerate(traces):
        for j, y in enumerate(traces):
            dist[x,y] = dtw.edit_distance(traces[x], traces[y])
            mat[i, j] = dist[x, y]

    Y, s = mds.mds(mat)
    return Y, s, [float(x) for x in traces.keys()]

# File Conversion to PNG (much smaller!)

# convert_pickle_file('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_20141107_00_11_1415340000.pck',
#                     '/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_baseline_chng.pck')

# convert_pickle_file('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_20141103_00_11_1414994400.pck',
#                     '/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_png.pck')

convert_pickle_file('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_20141110_00_11_1415599200.pck',
                    '/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_baseline_normal.pck')

# File Conversion Extract Results

# convert_pickle_results('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_png.pck',
#                        '/Users/stober/wrk/gazebo_ros_vizmc/data/results_alt.pck')

# convert_pickle_results('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_baseline_chng.pck',
#                        '/Users/stober/wrk/gazebo_ros_vizmc/data/results_baseline_chng.pck')

if False:

    # Y, s, lbls = sensorimotor_embedding_results('/Users/stober/wrk/gazebo_ros_vizmc/data/results_baseline_chng.pck')
    Y, s, lbls = sensorimotor_embedding_results('/Users/stober/wrk/gazebo_ros_vizmc/data/results_alt.pck')
    pylab.scatter(Y[:, 0], [float(x) for x in lbls])  #, Y[:, 0])
    pylab.title('Sensorimotor Embedding')
    pylab.xlabel('Sensorimotor Space')
    pylab.ylabel('Track Position (Meters)')
    pylab.show()


    # pickle.dump(zip([float(x) for x in traces.keys()], Y[:, 0]), open('se_data_alt.pck', 'w'), pickle.HIGHEST_PROTOCOL)

    # pylab.savefig('se_vergence_alt.png')

    # print dist
        # gamma, pos, score = find_minimum(key, data)
        # # print gamma, pos, score
        # positions.append(pos)
        # gammas.append(gamma)



    # pylab.plot(positions, gammas)
    # pylab.title('Vergence Angles by Track Position')
    # pylab.xlabel('Track Position (Meters)')
    # pylab.ylabel('Vergence Angle (Radians)')
    # pylab.savefig('vergence_position_plot_alt.png')
    # data = pckopen('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_png.pck')
    # for d in data:
    #     if d['position'] == pos and d['gamma'] == gamma:
    #         pylab.imshow(convert_image_numpy(d, 'top_image'))
    #         pylab.show()
