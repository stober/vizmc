#! /usr/bin/env python
'''
@jstober
'''

import rospy
from gazebo_msgs.srv import SetModelState, GetModelState
from gazebo_msgs.msg import ModelState
import tf
from dynamic_reconfigure.server import Server
from gazebo_ros_vizmc.cfg import ObsConfig
rospy.init_node('obs_controller')

MODELNAME = 'observer'
set_model_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
get_model_state = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)

# gazebo_msgs/ModelState model_state
#   string model_name
#   geometry_msgs/Pose pose
#     geometry_msgs/Point position
#       float64 x
#       float64 y
#       float64 z
#     geometry_msgs/Quaternion orientation
#       float64 x
#       float64 y
#       float64 z
#       float64 w
#   geometry_msgs/Twist twist
#     geometry_msgs/Vector3 linear
#       float64 x
#       float64 y
#       float64 z
#     geometry_msgs/Vector3 angular
#       float64 x
#       float64 y
#       float64 z
#   string reference_frame
# ---
# bool success
# string status_message

def callback(config, level):
    # this resets all the parameters - so we don't have the option to just toggle the action here

    rospy.loginfo(config)
    model_state_resp = get_model_state(MODELNAME, "")
    rospy.loginfo(model_state_resp)
    #
    # print dir(model_state)

    model_state = ModelState()
    model_state.reference_frame = "world"
    model_state.model_name = MODELNAME

    model_state.pose.position.x = config['x']
    model_state.pose.position.y = config['y']
    model_state.pose.position.z = config['z']

    ql = tf.transformations.quaternion_from_euler(config['yaw'], config['pitch'], config['roll'])
    model_state.pose.orientation.x = ql[0]
    model_state.pose.orientation.y = ql[1]
    model_state.pose.orientation.z = ql[2]
    model_state.pose.orientation.w = ql[3]

    set_model_state(model_state)

    rospy.loginfo(model_state)

    return config

if __name__ == '__main__':
    rate = rospy.Rate(10)
    config_srv = Server(ObsConfig, callback)
    rospy.spin()
