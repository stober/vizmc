#!/usr/bin/env python
__author__ = 'stober'

"""
Run the vergence policy on the robot to generate traces.
"""

import threading
import rospy
from sensor_msgs.msg import Image
from gazebo_ros_vizmc.msg import VergenceScore
from gazebo_ros_vizmc.srv import MCSetVergence, MCSetPosition, MCToggle
from PIL import Image as pimage
import numpy as np
import cPickle as pickle
import time
from datetime import date

class VergenceController(threading.Thread):

    def __init__(self):
        super(VergenceController, self).__init__()
        today = date.today().strftime('%Y%m%d_%H_%m_%s')
        self.testfile = open('testfile_{}.pck'.format(today),'w')

        rospy.init_node('vergence_controller')

        self.position = 0
        self.left_lock = threading.Lock()
        self.right_lock = threading.Lock()
        self.obs_lock = threading.Lock()
        self.exit_event = threading.Event()

        self.rate = rospy.Rate(1)  # 100 hz

        self.left_header = None
        self.right_header = None
        self.obs_header = None
        self.right_image = None
        self.left_image = None
        self.obs_image = None

        self.left_processed = None
        self.right_processed = None

        self.values = np.zeros((2,640))

        self.ylimits = (220, 230)  # 10 pixel band

        self.score = None

        self.score_publisher = rospy.Publisher('/vergence/score', VergenceScore)

        rospy.wait_for_service('/vizmc/set_vergence')
        rospy.wait_for_service('/vizmc/toggle_mc')
        rospy.wait_for_service('/vizmc/set_position')
        self.set_vergence = rospy.ServiceProxy('/vizmc/set_vergence', MCSetVergence)
        self.set_position = rospy.ServiceProxy('/vizmc/set_position', MCSetPosition)
        self.toggle_mc = rospy.ServiceProxy('/vizmc/toggle_mc', MCToggle)

        left_camera = 'left_camera'  # 'top_camera'
        right_camera = 'right_camera'  # 'bottom_camera'

        rospy.Subscriber('/{}/image_raw'.format(left_camera), Image, self.left_callback)
        rospy.Subscriber('/{}/image_raw'.format(right_camera), Image, self.right_callback)
        rospy.Subscriber('/obs_camera/image_raw', Image, self.obs_callback)

    def transform(self, img, target='PIL'):
        if target == 'PIL':
            assert img.encoding in ('rgb8','mono8')
            size = (img.width,img.height)

            if img.encoding == 'rgb8':
                return pimage.frombuffer("RGB", size, img.data, 'raw', "RGB", 0, 1)
            else:
                return pimage.frombuffer("L", size, img.data, 'raw', "L", 0, 1)

        elif target == 'numpy':
            assert img.encoding in ('rgb8', 'mono8')
            if img.encoding == 'mono8':
                arr = np.fromstring(img.data, dtype=np.uint8).reshape((img.height, img.width))
            else:
                arr = np.fromstring(img.data, dtype=np.uint8).reshape((img.height, img.width, 3))

            return arr

    def left_callback(self, msg):
        with self.left_lock:
            self.left_header = msg.header
            self.left_image = self.transform(msg, target='numpy')

    def right_callback(self, msg):
        with self.right_lock:
            self.right_header = msg.header
            self.right_image = self.transform(msg, target='numpy')

    def obs_callback(self, msg):
        with self.obs_lock:
            self.obs_header = msg.header
            self.obs_image = self.transform(msg, target='numpy')

    def cut(self):
        with self.left_lock:
            if self.left_image is None:
                pass
            else:
                try:
                    section = self.left_image[self.ylimits[0]:self.ylimits[1],:]
                    self.left_processed = np.zeros(section.shape)
                    self.left_processed[section < 160] = 1.0
                except TypeError as e:
                    rospy.logerr(e)
                    rospy.logerr(type(self.left_image))

        with self.right_lock:
            if self.right_image is None:
                pass
            else:
                try:
                    section = self.right_image[self.ylimits[0]:self.ylimits[1],:]
                    self.right_processed = np.zeros(section.shape)
                    self.right_processed[section < 160] = 1.0
                except TypeError as e:
                    rospy.logerr(e)
                    rospy.logerr(type(self.right_image))

    def difference(self):
        """
        Compute the distance between two arrays (as the score of the vergence).
        """
        if self.left_processed is not None and self.right_processed is not None:
            self.score = np.linalg.norm(self.left_processed - self.right_processed)
            self.score_publisher.publish(self.score)

    def featurize(self):
        # we only need to compute the mode for the left due to the symmetry of the task
        if self.left_processed is not None and self.right_processed is not None:
            # don't do arg max here (not stable) find center of mass
            weights = np.sum(self.left_processed, axis=0)
            diffs = np.zeros(640)
            for i in range(640):
                diffs[i] = np.abs(np.sum(weights[:i]) - np.sum(weights[i:]))
            return np.argmin(diffs)

    def verify_time(self, wait=1e9):
        now_raw = rospy.get_rostime()
        now = int(now_raw.secs * 1e9 + now_raw.nsecs + wait)

        left_time = 0.0
        right_time = 0.0

        with self.left_lock:
            left_time = int(self.left_header.stamp.secs * 1e9 + self.left_header.stamp.nsecs)

        with self.right_lock:
            right_time = int(self.right_header.stamp.secs * 1e9 + self.right_header.stamp.nsecs)

        print now, left_time, right_time

        cnt = 0
        while left_time < now or right_time < now:

            if rospy.is_shutdown():
                return

            # tight loop
            cnt += 1
            with self.left_lock:
                left_time = int(self.left_header.stamp.secs * 1e9 + self.left_header.stamp.nsecs)

            with self.right_lock:
                right_time = int(self.right_header.stamp.secs * 1e9 + self.right_header.stamp.nsecs)


        rospy.loginfo("cnt: {}".format(cnt))
        return

    def vergence_policy(self, feature):
        if feature > 320:
            return -0.005
        elif feature == 320:
            return 0.0
        else:
            return 0.005

    def rotation_policy(self):
        """
        Set the policy to the learned policy.
        """
        gamma = np.arange(-0.1, 0.6, 0.01) * -1.0
        for g in gamma:
            yield g

    def position_policy(self):
        positions = np.arange(-1.9, 1.9, 0.1)
        for pos in positions:
            yield pos

    def run_gamma_range(self):
        nsteps = 0
        # time.sleep(10)  # let everything start up
        time.sleep(1)

        for pos in self.position_policy():
            for gamma in self.rotation_policy():

                self.set_position(pos, True)
                self.set_vergence(gamma, True)
                self.rate.sleep()

                self.verify_time()
                rospy.loginfo('gamma: {} position: {}'.format(gamma, pos))

                if self.exit_event.is_set():
                    break

                if not self.testfile.closed:
                    pickle.dump({'position': pos,
                                 'nsteps': nsteps,
                                 'left_image': self.left_image,  # top_image
                                 'right_image': self.right_image,  # bottom_image
                                 'obs_image': self.obs_image,
                                 'gamma': gamma}, self.testfile, pickle.HIGHEST_PROTOCOL)

                nsteps += 1

        rospy.loginfo('Finished')

    def run_se(self):
        self.current_gamma = 0.0

        positions = iter([-1.0, -0.5, 0.0, 0.5, 1.0])
        positions = iter(np.arange(-1.9, 1.9, 0.1))
        position = positions.next()
        self.set_position(position, True)

        nsteps = 0
        time.sleep(10)
        while not self.exit_event.is_set():
            try:

                self.rate.sleep()

                # self.cluster()
                self.cut()
                self.difference()
                feature = self.featurize()


                gamma_delta = self.vergence_policy(feature)
                self.current_gamma += gamma_delta

                self.set_vergence(self.current_gamma, True)

                self.verify_time() # make sure the camera image comes from after the vergence action

                rospy.loginfo('gamma: {} difference: {} feature: {} position: {}'.format(self.current_gamma,
                                                                                         self.score,
                                                                                         feature,
                                                                                         position))

                nsteps += 1

                if not self.testfile.closed:
                    pickle.dump({'position': position,
                                 'nsteps': nsteps,
                                 'left_image': self.left_image,
                                 'left_processed': self.left_processed,
                                 'right_image': self.right_image,
                                 'right_processed': self.right_processed,
                                 'obs_image': self.obs_image,
                                 'gamma': self.current_gamma,
                                 'score': self.score,
                                 'feature': feature}, self.testfile, pickle.HIGHEST_PROTOCOL)

                if np.abs(feature - 320) <= 2:
                    try:
                        rospy.loginfo('Switching position!')
                        position = positions.next()
                        self.set_position(position, True)
                        self.current_gamma = 0.0
                        self.set_vergence(0.0, True)
                        time.sleep(5)
                    except:
                        rospy.loginfo('Finished')
                        break


            except rospy.ROSInterruptException:
                break # shutting down

    def run(self):
        self.run_gamma_range()

def main():


    v = VergenceController()
    v.toggle_mc(False)
    v.start()

    rospy.spin()

    v.exit_event.set()
    v.join()

if __name__ == '__main__':

    main()

# TODO: example policy in action

# y = img.reshape((480,640,3)) 192.168.214.136
