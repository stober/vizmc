__author__ = 'Jeremy Stober <stober@gmail.com>'

import numpy as np
import cPickle as pickle
import pylab
from matplotlib import animation, gridspec
import skimage.io as io
import skimage
from operator import itemgetter
import itertools
import pyrouette.mds
#print io.available_plugins

class SquareCamera(object):
    """
    Yet another roving eye robot.
    """

    def __init__(self, img, idim, x=0, y=0):
        self.idim = idim
        self.img = img
        self.x, self.y = x, y

    def move(self, dx, dy):
        self.x += dx
        self.y += dy

    def move_absolute(self, x, y):
        self.x = x
        self.y = y


    def capture(self):
        return self.img[self.y:self.y+self.idim, self.x:self.x+self.idim]  # image indexing conventions

class BoxField(object):

    def __init__(self, i, j, w, h):
        self.coords = (i, j)
        self.extent = (w, h)

    @staticmethod
    def activation(data):
        return np.mean(data.ravel())

    def project(self, img):
        (i, j) = self.coords
        (w, h) = self.extent
        return self.activation(img[i:i+h, j:j+w])


class SquareFovea(object):
    """
    Simplified fovea filter.
    """
    def __init__(self, extents, idim, fdim=None):
        self.extents = extents
        self.idim = idim
        self.fields = []
        if fdim is None:
            fdim = self.idim / max(self.extents)
        self.fdim = fdim
        self.center = self.idim / 2
        self.layer_boxes = []

        for ext in self.extents:
            assert self.idim % ext == 0, "Extents must divide the image size."

            start = self.center - self.fdim * ext / 2
            end = start + self.fdim * ext

            self.layer_boxes.append((start, end))
            for i in range(start, end, ext):
                for j in range(start, end, ext):
                    self.fields.append(BoxField(i, j, ext, ext))

    def project(self, img):
        assert img.shape == (self.idim, self.idim)
        return np.array([field.project(img) for field in self.fields])

    def boxes(self, img, nlayers="all"):
        """
        Show the layer boxes.

        :param img:
        :return:
        """
        nimg = img.copy()
        if nlayers is "all":
            layers = self.layer_boxes
        else:
            layers = self.layer_boxes[:1]

        for start, end in layers:
            nimg[start, start:end-1] = 255.0
            nimg[end - 1, start:end-1] = 255.0
            nimg[start:end-1, start] = 255.0
            nimg[start:end-1, end - 1] = 255.0

        return nimg


    def image(self, act, avg=False):
        assert len(act) == len(self.fields), "Incomparable lengths."
        img = np.zeros((self.idim, self.idim))
        counts = np.zeros((self.idim, self.idim))
        for a, field in zip(act, self.fields):
            i, j = field.coords
            w, h = field.extent
            img[i:i+h, j:j+w] += a
            counts[i:i+h, j:j+w] += 1
        if avg:
            return img / counts
        else:
            return img

    def compare(self, imga, imgb):
        acta = self.project(imga)
        actb = self.project(imgb)
        acta /= np.linalg.norm(acta)
        actb /= np.linalg.norm(actb)
        return np.linalg.norm(acta - actb)


def threshold(img, idim=480, t=155):
    h, w = img.shape
    assert h >= idim and w >= idim

    centerh = h / 2
    offset = idim / 2
    centerw = w / 2
    img = img[centerh - offset:centerh + offset, centerw - offset:centerw + offset]
    img[img < t] = 1.0
    img[img >= t] = 0.0
    return img


def create_test_movie():
    rdata = []
    ldata = []
    fp = open('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_20141009_00_10_1412830800.pck')
    while True:
        try:
            rdata.append(threshold(pickle.load(fp)['right_image']))
            ldata.append(threshold(pickle.load(fp)['left_image']))
        except EOFError:
            break
    return rdata, ldata


def fovea_filter(img, fovea):
    return fovea.image(fovea.project(img), avg=True)

def imshow_animate_fovea(imgs, fovea):
    # fig, axes = pylab.subplots(2, 2, figsize=(12, 6),
    #                      subplot_kw={'xticks': [], 'yticks': []})

    gs = gridspec.GridSpec(2, 2)
    ax1 = pylab.subplot(gs[0, 0])
    ax2 = pylab.subplot(gs[0, 1])
    ax3 = pylab.subplot(gs[1, :])

    r, l = imgs[0]

    diffs = []
    initialr = fovea_filter(r, fovea)
    initiall = fovea_filter(l, fovea)
    diffs.append(fovea.compare(r, l))

    iml = ax1.imshow(initiall)
    imr = ax2.imshow(initialr)
    plt = ax3.plot(range(len(diffs)), diffs)[0]

    ax1.set_title('Left Image')
    ax2.set_title('Right Image')

    ax3.set_title('Normalized Image Difference')
    ax3.set_ylim(0, 2.0)
    ax3.set_ylabel('Image Difference')
    ax3.set_xlabel('Time')
    ax3.set_xlim(0, len(imgs))

    def animate(img):
        r, l = img
        framer = fovea_filter(r, fovea)
        framel = fovea_filter(l, fovea)
        diffs.append(fovea.compare(r, l))
        iml.set_data(framel)
        imr.set_data(framer)
        plt.set_data(range(len(diffs)), diffs)

    return animation.FuncAnimation(pylab.gcf(), animate, frames=imgs)

def test():
    rdata, ldata = create_test_movie()

    # img = np.load('../data/test_img.npy')
    # pylab.imshow(data[0])
    # pylab.show()

    fovea = SquareFovea([1, 2, 4, 12, 24], 480, fdim=20)
    comparisons = [fovea.compare(r, l) for r, l in zip(rdata, ldata)]
    min_indx = np.argmin(comparisons)
    # print min_indx
    r, l = rdata[min_indx], ldata[min_indx]

    anime = imshow_animate_fovea(zip(rdata, ldata), fovea)
    anime.save('fovea_animation.mp4', fps=5)#, extra_args=['-vcodec', 'libx264'])
    # pylab.show()

    # act = fovea.project(r)
    # img = fovea.image(act)
    # pylab.imshow(img)
    # pylab.show()
    # pylab.imshow(r)
    # pylab.show()

imread = io.imread
imsave = io.imsave
rgb2gray = skimage.color.rgb2gray

def test3():
    dist = [(i, j, np.abs(x - y)) for (i, x), (j, y) in itertools.product(enumerate([125, 106, 75]), repeat=2)]
    d = np.zeros((3, 3))
    for i, j, v in dist:
        d[i, j] = v

    print pyrouette.mds.mds(d, dimensions=1)

    dl = imread('/Users/stober/wrk/gazebo_ros_vizmc/data/Aloe/disp1.png')
    dr = imread('/Users/stober/wrk/gazebo_ros_vizmc/data/Aloe/disp5.png')

    # 560 (445, 0.14134536368523157) 445 + 240 # 115  125
    # 710 (576, 0.11422439933196227) 576 + 240 # 134  106
    # 900 (735, 0.090043450885472331) 735 + 240 # 165 75

    print dr[240, 560]
    print dr[240, 710]
    print dr[240, 900]

def run_vergence_experiment(start_pos, gen_plots=False):
    l = rgb2gray(imread('/Users/stober/wrk/gazebo_ros_vizmc/data/Aloe/view1.png'))
    r = rgb2gray(imread('/Users/stober/wrk/gazebo_ros_vizmc/data/Aloe/view5.png'))

    h, w = l.shape

    scl = SquareCamera(l, 480, x=start_pos-240)  #view1
    scr = SquareCamera(r, 480, x=start_pos-240)  #view5

    scl.move_absolute(0, 0)

    fovea = SquareFovea([1, 2, 4, 12, 24], 480, fdim=20)

    imgr = scr.capture()

    data = []
    for i in range(w - 480):
        scl.move(1, 0)
        imgl = fovea_filter(scl.capture(), fovea)
        data.append((i, fovea.compare(imgr, imgl)))

    x = sorted(data, key=itemgetter(1))
    print start_pos, x[0]

    # 560 (445, 0.14134536368523157) # 115
    # 710 (576, 0.11422439933196227) # 134
    # 900 (735, 0.090043450885472331) # 165

    if gen_plots:
        data = np.array(data)
        pylab.gcf().clear()
        pylab.plot(data[:, 0], data[:, 1])
        pylab.title('Right Image Fixed at 320')
        pylab.xlabel('Left Image Pixel Position.')
        pylab.ylabel('Normalized Fovea Difference')
        pylab.xlim((0,800))
        pylab.savefig('aloe_fovea_scan_{}.png'.format(start_pos))


def exp():
    for start_pos in [560, 710, 900]:
        run_vergence_experiment(start_pos)


def test2():
    # print io.plugins()
    # io.use_plugin('pil', 'imread')
    # io.use_plugin('matplotlib', 'imsave')

    l = rgb2gray(imread('/Users/stober/wrk/gazebo_ros_vizmc/data/Aloe/view1.png'))
    r = rgb2gray(imread('/Users/stober/wrk/gazebo_ros_vizmc/data/Aloe/view5.png'))

    # imsave('right_top.png', r[:480, :])
    # imsave('left_top.png', l[:480, :])

    h, w = l.shape

    scl = SquareCamera(l, 480, x=560-240)  #view1
    scr = SquareCamera(r, 480, x=560-240)  #view5

    fovea = SquareFovea([1, 2, 4, 12, 24], 480, fdim=20)

    imgl = scl.capture()
    imgr = scr.capture()

    # imsave('imgl_start.png', imgl)
    # imsave('imgr_start.png', imgr)

    imgl = fovea_filter(scl.capture(), fovea)
    imgr = fovea_filter(scr.capture(), fovea)

    imsave('imgl_start_fovea.png', imgl)
    imsave('imgr_start_fovea.png', imgr)

    # 446 versus 340 - First target

    first = (446, 0.10404757979459391)
    second = (399, 0.11669844243379464)

    # 710
    # 900

    scl.move_absolute(446, 0)
    # imsave('446.png', fovea.boxes(scl.capture(), "one"))
    scl.move_absolute(399, 0)
    # imsave('399.png', fovea.boxes(scl.capture(), "one"))

    # imsave('right.png', fovea.boxes(scr.capture(),"one"))

    # DONE: box option on save - mark center

    if False:  # full trace
        scl.move_absolute(0, 0)

        data = []
        for i in range(w - 480):
            scl.move(1, 0)
            imgl = fovea_filter(scl.capture(), fovea)
            data.append((i, fovea.compare(imgr, imgl)))

        x = sorted(data, key=itemgetter(1))
        data = np.array(data)
        pylab.plot(data[:, 0], data[:, 1])
        pylab.title('Right Image Fixed at 320')
        pylab.xlabel('Left Image Pixel Position.')
        pylab.ylabel('Normalized Fovea Difference')
        pylab.xlim((0,800))
        pylab.savefig('aloe_fovea_scan.png')


        print x

        # pylab.imshow(imgr)
        # pylab.show()


if __name__ == '__main__':
    # test2()
    # exp()
    test3()

