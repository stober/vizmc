__author__ = 'Jeremy Stober <stober@gmail.com>'

import math
import random as pr


class MountainCarController(object):

    def __init__(self):

        self.nsteps_action = 0
        self.mc_dynamics = True
        self.freq = math.pi * 0.25
        self.power = 0.001
        self.ratio = 0.0025
        self.shift = math.pi * 0.5
        self.position = 0.0
        self.velocity = 0.0
        self.action = 0

        self.hard_position_limits = (-2.0, 2.0)
        self.soft_velocity_limits = (-0.07, 0.07)

    def toggle_mc(self, request):
        self.mc_dynamics = request.toggle
        return self.mc_dynamics

    def get_mcstate(self):
        """
        A service version of the state information (useful for fastmode).
        """
        return self.position, self.velocity, self.action

    def run_service(self, action, nsteps):
        """
        Run mountain car for the requested number of steps as a service. Do not run this if running mountain car as a thread.
        """

        self.action = action

        for i in range(nsteps):
            self.update_dynamics()

        return self.position, self.velocity


    def reset(self, random=False):
        if random:
            self.position = pr.uniform(-2.0, 2.0)
            self.velocity = pr.uniform(-0.07,0.07)
            self.action = 1
        else:
            self.position = 0.0
            self.velocity = 0.0
            self.action = 0

    def update_dynamics(self):
        """
        The code that actually updates the state space.
        """
        if self.mc_dynamics == False:
            return # don't run mountaincar

        self.velocity = self.velocity + self.action * self.power + math.cos(self.freq * self.position + self.shift) * self.ratio

        self.position = self.position + self.velocity

        if self.position >= self.hard_position_limits[1]:
            self.position = self.hard_position_limits[1]
            self.velocity = 0.0 # reset velocity on hard stop

        if self.position <= self.hard_position_limits[0]:
            self.position = self.hard_position_limits[0]
            self.velocity = 0.0
