#! /usr/bin/env python
'''
@jstober

DEPRECATED -- DEPRECATED -- DEPRECATED
'''

import rospy
from pyrouette.td import ActorCriticCmac
from gazebo_ros_vizmc.msg import MCState
from gazebo_ros_vizmc.srv import MCSetAction, MCReset, MCRunService, MCStateService
import threading
import numpy as np
import cPickle as pickle
import time
import bz2
import os
import re
import random
import click
import pdb

class ModeState(object):
    """
    Simple state machine for accessing and updating the mode of the learner.
    """

    def __init__(self):
        self.__mode = 0
        self.update_mode = threading.Lock()

    def set_operate(self):
        with self.update_mode:
            self.__mode = 0

    def set_train(self):
        with self.update_mode:
            self.__mode = 1

    def set_test(self):
        with self.update_mode:
            self.__mode = 2

    def get_mode(self):
        mode = ""
        with self.update_mode:
            if self.__mode == 0:
                mode = 'operate'
            elif self.__mode == 1:
                mode = 'train'
            elif self.__mode == 2:
                mode = 'test'
        return mode


def load_se(filename):
    fp = open(filename)
    sedata = []
    while True:
        try:
            sedata.append(pickle.load(fp))
        except:
            break
    return sedata


def find_closest(val, arr):
    return np.argmin(np.abs(arr - val))

class RandomLearner(object):

    def __init__(self, nactions):
        self.nactions = nactions

    def best(self, *args, **kwargs):
        return random.choice(range(self.nactions))


class MCLearner(threading.Thread):

    def __init__(self):
        super(MCLearner, self).__init__()
        rospy.init_node('mc_learner')

        self.exit_event = threading.Event()
        self.position = 0.0
        self.velocity = 0.0
        self.mode_state = ModeState()

        self.learner = ActorCriticCmac(3, 0.05, 1.0, 0.95, 0.8, 0.9)
        self.learner.actor.init = 1.0
        self.learner.critic.init = 1.0
        self.save_directory = ""

        self.training_cnt = 0

        self.mcstate_lock = threading.Lock()

        rospy.Subscriber('/vizmc/mcstate', MCState, self.mcstate_callback)

        self.set_action = rospy.ServiceProxy('/vizmc/set_action', MCSetAction)
        self.rate = rospy.Rate(1)

        self.reset = rospy.ServiceProxy('/vizmc/reset', MCReset)

        self.run_service = rospy.ServiceProxy('/vizmc/run_service', MCRunService)
        self.get_mcstate = rospy.ServiceProxy('/vizmc/get_mcstate', MCStateService)

    def load_se_data(self, filename):
        """
        This file contains data of the following type:
        pickle.dump((i, position, Y[i, 0], pdata[0]), outfp, pickle.HIGHEST_PROTOCOL)

        Basically this is the output of sensorimotor embedding.
        """
        self.se = load_se(filename)
        self.fast_se = {se[1]: se[2] for se in self.se}
        self.previous_se = -1

    def mcstate_callback(self, data):
        with self.mcstate_lock:
            self.position = data.position
            self.velocity = data.velocity

    def reward(self):
        if self.position > 1.9:
            return 1000.0
        else:
            return 0.0

    def is_finished(self):
        return self.position > 1.9

    def test_policy(self):
        """
        Run a particular policy.
        """
        if self.velocity > 0:
            self.set_action(1)
        else:
            self.set_action(-1)

    def observe(self):
        with self.mcstate_lock:
            pos, vel = self.position, self.velocity
        return np.array([pos, vel])


    def test(self):
        # TODO fix this or delete
        self.reset(random=False)
        self.testing_cnt = 0

        while self.mode_state.get_mode() == 'test':
            self.testing_cnt += 1
            obs = self.observe()

            reward = self.reward()
            if self.is_finished():
                rospy.loginfo('Learned!')
            if self.testing_cnt > 10000:
                rospy.loginfo('Testing taking too long!')
                self.mode_state.set_train() # go back to training

            a = self.learner.best(obs) - 1
            self.set_action(a)
            self.rate.sleep()

    def fast_observe(self):
        response = self.get_mcstate()
        return self.state2array(response)

    def find_se_pos(self, pos):
        keys = self.fast_se.keys()
        marg = find_closest(pos, np.array(keys))
        se_pos = self.fast_se[keys[marg]]
        return se_pos

    def fast_se_observe(self, update=True):
        # return position using se state in (slow) version we'd compare the left_camera images - here we'll just
        # use the se state associated with the position (which comparing images would give us anyway)
        response = self.get_mcstate()
        [pos, vel] = self.state2array(response)
        se_pos = self.find_se_pos(pos)
        se_vel = se_pos - self.previous_se
        if update == True:
            self.previous_se = se_pos
        return np.array([se_pos, se_vel])

    def fast_finished(self):
        response = self.get_mcstate()
        return response.position > 1.9

    def fast_reward(self):
        response = self.get_mcstate()
        if response.position > 1.9:
            return 10.0
        else:
            return 0.0

    def state2array(self, response):
        return np.array([response.position, response.velocity])

    def fast_se_train(self, max_cnt=100000):
        while self.training_cnt < max_cnt:
            self.training_cnt += 1

            if self.training_cnt % 1000 == 0:
                rospy.loginfo('Training count: {}'.format(self.training_cnt))
                rospy.loginfo('Resetting to random position!')
                self.save()
                self.reset(random=True)

            if self.training_cnt % 10000 == 0:
                rospy.loginfo('Start test')
                avg_cnt = self.fast_test(save_traces=True)
                rospy.loginfo('Average cnt: {}'.format(avg_cnt))
                continue # skip training

            pvector = self.fast_se_observe()
            a = self.learner.best(pvector)
            response = self.run_service(action=a-1, nsteps=10)
            vector = self.fast_se_observe(False)
            # vector = np.array([response.position, response.velocity])
            action = a

            reward = self.fast_reward()
            if self.fast_finished():
                rospy.loginfo('Goal reached!')
                self.reset(random=True)

            if self.training_cnt % 100 == 0:
                rospy.loginfo("{} {} {} {} {}".format(str(pvector), str(vector), action, reward, 10))

            self.learner.train(pvector, action, reward, vector, self.learner.best(vector))

    def fast_train(self, nsteps=10, max_cnt=100000):
        training_actions = []
        while self.training_cnt < max_cnt:
            self.training_cnt += 1

            if self.training_cnt % 1000 == 0:
                rospy.loginfo('Training count: {}'.format(self.training_cnt))
                rospy.loginfo('Resetting to random position!')
                rospy.loginfo('Actions: {}'.format(training_actions))
                self.learner.diagnostics()
                training_actions = []
                self.save()
                self.reset(random=True)

            if self.training_cnt % 10000 == 0:
                rospy.loginfo('Start test')
                avg_cnt = self.fast_test(nsteps=nsteps, save_traces=True, observer=self.fast_observe, learner=self.learner)
                rospy.loginfo('Average cnt: {}'.format(avg_cnt))
                continue # skip training

            pvector = self.fast_observe()
            a = self.learner.best(pvector)
            training_actions.append(a)
            response = self.run_service(action=a-1, nsteps=nsteps)
            vector = np.array([response.position, response.velocity])
            action = a

            reward = self.fast_reward()
            if self.fast_finished():
                rospy.loginfo('Goal reached!')
                self.reset(random=True)

            if self.training_cnt % 100 == 0:
                rospy.loginfo("{} {} {} {} {}".format(str(pvector), str(vector), action, reward, 10))

            # pdb.set_trace()
            self.learner.train(pvector, action, reward, vector, self.learner.best(vector))

            # print out some debug info
            #

    def fast_test(self, nsteps=10, ntrials = 10, save_traces=False, observer=None, learner=None):

        if observer is None:
            observer = self.fast_observe

        if learner is None:
            learner = self.learner

        tests = []
        avg_cnt = 0
        for i in range(ntrials):
            if i % 10 == 0:
                rospy.loginfo('testing trial {}'.format(i))
            self.reset(random=True)
            cnt = 0
            test = []
            trail_actions = []
            while not self.fast_finished() and cnt < 1000:
                vector = observer()
                a = learner.best(vector)
                response = self.run_service(action=a-1, nsteps=nsteps)
                test.append((vector, a, self.state2array(response)))
                cnt += 1

            avg_cnt += cnt
            tests.append(test)

        if save_traces:
            fp = self.create_fp('test_traces')
            pickle.dump(tests, fp, pickle.HIGHEST_PROTOCOL)
            fp.close()

        result = float(avg_cnt) / float(ntrials)
        rospy.loginfo('TRIAL RESULT: {}'.format(result))
        return result

    def train(self):
        while self.mode_state.get_mode() == 'train':
            self.training_cnt += 1

            if self.training_cnt % 1000 == 0:
                rospy.loginfo('Training count: {}'.format(self.training_cnt))
                rospy.loginfo('Resetting to random position!')
                self.save()
                self.reset(random=True)

            if self.training_cnt % 10000 == 0:
                rospy.loginfo('Start test')
                self.mode_state.set_test()
                continue # skip training

            obs = self.observe()
            a = self.learner.best(obs)
            # rospy.loginfo('action is: {}'.format(a))
            nsteps = self.set_action(action=a-1)
            pvector = obs
            action = a

            self.rate.sleep()  # TODO: step here?

            reward = self.reward()
            if self.is_finished():
                rospy.loginfo('Goal reached!')
                self.reset(random=True)

            vector = self.observe()
            rospy.loginfo("{} {} {} {} {}".format(str(pvector), str(vector), action, reward, nsteps))
            self.learner.train(pvector, action, reward, vector, self.learner.best(vector))

    def operate(self):
        while self.mode_state.get_mode() == 'operate':
            obs = self.observe()
            a = self.learner.best(obs)
            self.set_action(action=a-1)
            self.rate.sleep()

    def set_save_directory(self, directory):
        self.save_directory = directory

    def create_fp(self,prefix):
        return bz2.BZ2File(os.path.join(self.save_directory, "{}_{}.pck".format(prefix, int(time.time()))), 'w')

    def save(self):
        """
        Saves the state of the current leaner.
        """
        if self.save_directory:
            fp = self.create_fp('chk_pt_learner')
            pickle.dump(self.learner, fp, pickle.HIGHEST_PROTOCOL)
            fp.close()
        else:
            rospy.loginfo('Save directory not set! Not checkpointing...')

    def load(self, filename):
        fp = bz2.BZ2File(filename)
        self.learner = pickle.load(fp)
        fp.close()

    def run(self):
        while not self.exit_event.is_set():
            try:

                # TODO make exit a mode
                # we switch between different loops in the run method
                mode = self.mode_state.get_mode()
                if mode == 'train':
                    self.train()
                elif mode == 'test':
                    self.test()
                elif mode == 'operate':
                    self.operate()
                else:
                    raise Exception('Unknown mode state! {}'.format(mode))

            except rospy.ServiceException as exc:
                rospy.loginfo('set_action failed! Thread exiting!')
                break

            try:
                self.rate.sleep()
            except:
                pass

@click.command()
@click.option('--fast', is_flag=True, help='Enable fast mode for faster training when not connected to Gazebo.')
@click.option('--learn', is_flag=True, help='Learn a policy.')
@click.option('--test', is_flag=True, help='Test learning.')
@click.option('--directory', type=click.Path(exists=True, readable=True, file_okay=False), help='Directory of checkpoint files.')
@click.option('--mode', type=click.Choice(['se', 'random', 'normal']), help='What type of features/learner to use.')
@click.option('--se-data', type=click.Path(exists=True, readable=True, dir_okay=False), help="The file with sensorimotor embedding data.")
@click.option('--load', help='Load a specific learner (useful for actually running on the robot).')
@click.option('--nsteps', default=10, help='The length of the action.')
def cli(fast, learn, test, directory, mode, se_data, load, nsteps):
    learner = MCLearner()

    if directory:
        learner.set_save_directory(directory)

    se = mode == 'se'
    random = mode == 'random'
    normal = mode == 'normal'

    if se:
        learner.load_se_data(se_data)

    if fast:
        if learn:
            learner.fast_train(nsteps=nsteps)
        if test:
            if random:
                rospy.loginfo('Running in test mode with random agent.')
                results = []
                for i in range(20):
                    results.append(learner.fast_test(nsteps, observer=learner.fast_observe, learner=RandomLearner(3)))
            if normal or se:
                files = [file for file in os.listdir(directory) if re.match('chk_pt_learner_(\d+).pck', file)]
                results = []
                if normal:
                    observer = learner.fast_observe
                elif se:
                    observer = learner.fast_se_observe

                for file in files:
                    rospy.loginfo('Testing file {}'.format(file))
                    learner.load(os.path.join(directory, file))
                    learner.fast_test(nsteps, observer=observer)

            fp = open(os.path.join(directory, "test_results.txt"), "w")
            for r in results:
                fp.write(str(r))
                fp.write('\n')
            fp.close()

    else:
        if load:
            learner.load(os.path.join(directory, load))

        learner.start()  # run thread

        r = rospy.Rate(1)

        while not rospy.is_shutdown() and learner.is_alive():
            r.sleep()

        learner.exit_event.set()

if __name__ == '__main__':
    cli()