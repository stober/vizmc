__author__ = 'Jeremy Stober <stober@gmail.com>'

import cPickle as pickle
import pylab
import numpy as np
# from retina import Retina, BoxLayers, BoxField, SimpleTrain, SimpleBinaryField
from fovea import SquareFovea

trace = '/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_20141009_00_10_1412830800.pck'
x = []
fp = open(trace)
while True:
    try:
        x.append(pickle.load(fp))
    except:
        break

# class ReverseBinaryActivation(object):
#
#     def activation(self,stimulation):
#         """
#         A threshold activation method.
#         """
#         return int((stimulation / self.size) < self.threshold)

img = x[0]['right_image'][:480, :480]
img[img < 155] = 1.0
img[img >= 155] = 0.0

# pylab.imshow(img)
# pylab.imshow(img[220:260, 220:260])
# pylab.show()

# class SimpleBinaryField(ReverseBinaryActivation, SimpleTrain, BoxField):
#     pass



retina = Retina(BoxLayers([1, 2, 4, 12, 24], 5, 480, rf=SimpleBinaryField, threshold=0.5))

# # pylab.imshow(img)
#

retina.project(img, normalize=True)
print retina.states()

# sumstate = np.zeros((480,480))
# print retina.expand(0)

# pylab.show()
