#!/usr/bin/env python
__author__ = 'Jeremy Stober <stober@gmail.com>'

import cPickle as pickle
import pylab as pl
import numpy as np
from pyrouette import dtw
from pyrouette import mds
import itertools

def find_position(data, position):
    return [d for d in data if d['position'] == position]

def view_image(img):
    pl.imshow(img)
    pl.show()

if False:
    fp = open('../data/testfile_20140909_00_09_1410238800.pck')
    data = []
    while True:
        try:
            data.append(pickle.load(fp))
        except:
            break
    
    positions = [d['position'] for d in data]
    for p in sorted(set(positions)):
        print p, len([d for d in data if d['position'] == p])

    print data[-1]['left_image']
    pl.imshow(data[-1]['left_image'])
    pl.savefig('test.png')

    # pl.imshow(data[0]['left_processed'])
    # pl.savefig('test2.png')


if True:
    fp = open('../data/testfile_20140821.pck')
    fp = open('../data/testfile_20140909_00_09_1410238800.pck')
    data = []
    while True:
        try:
            data.append(pickle.load(fp))
        except:
            break

    positions = [-1.0, -0.5, 0.0, 0.5, 1.0]
    positions = sorted(set([d['position'] for d in data]))
    traces = {}
    for position in positions:
        print position, len(find_position(data, position))
        traces[position] = np.ones(len(find_position(data, position)))  # all the actions here are the same

    print traces
    mat = np.zeros((len(traces), len(traces)))
    for i, j in itertools.product(range(len(positions)), repeat=2):
        x, y = positions[i], positions[j]
        print x, y, len(traces[x]), len(traces[y]), dtw.edit_distance(traces[x], traces[y])
        mat[i, j] = dtw.edit_distance(traces[x], traces[y])
        mat[j, i] = mat[i, j]

    Y, s = mds.mds(mat)

    outfp = open('se.pck', 'w')

    for i, position in enumerate(positions):
        pdata = find_position(data, position)
        pickle.dump((i, position, Y[i, 0], pdata[0]), outfp, pickle.HIGHEST_PROTOCOL)

    pl.scatter(Y[:, 0], Y[:, 1])
    pl.title('Sensorimotor Embedding Applied to Vergence')
    pl.savefig('vergence.png')



# scores = [d['score'] for d in data]
# features = [d['feature'] for d in data]
# gammas = [d['gamma'] for d in data]

# # trace file contain 2 cycles

# # creates the trace file
# pickle.dump((scores, features, gammas), open('../data/trace2.pck','w'), pickle.HIGHEST_PROTOCOL)
