__author__ = 'Jeremy Stober <stober@gmail.com>'

#! /usr/bin/env python
'''
@jstober
'''

from pyrouette.td import ActorCriticCmac, SarsaCmac
import numpy as np
import cPickle as pickle
import time
import bz2
import random
import click
from mc_dynamics import MountainCarController
from mc_ros_bridge import RosMountainCarController  # TODO: make this optional?
import logging
from collections import Counter
import json
from collections import deque
import logging.handlers

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
weblogger = logging.getLogger('web')
weblogger.propagate = False
weblogger.addHandler(logging.handlers.HTTPHandler('localhost:5000','/log', method='POST'))

def create_fp(dirname, basename, extension='pck', compress=True, timestamp=True, mode='w'):
    if timestamp:
        name = "{}_{}".format(time.time(), basename)
    else:
        name = basename
    filename = "{}/{}.{}".format(dirname, name, extension)
    if compress:
        return bz2.BZ2File(filename, mode)
    else:
        return open(filename, mode)

class ModeState(object):
    """
    Simple state machine for accessing and updating the mode of the learner.
    """

    def __init__(self):
        self.__mode = 0
        
    def set_operate(self):
        self.__mode = 0

    def set_train(self):
        self.__mode = 1

    def set_test(self):
        self.__mode = 2

    def get_mode(self):
        mode = ""
        if self.__mode == 0:
            mode = 'operate'
        elif self.__mode == 1:
            mode = 'train'
        elif self.__mode == 2:
            mode = 'test'
        return mode


def load_se(filename):
    fp = open(filename)
    sedata = []
    while True:
        try:
            sedata.append(pickle.load(fp))
        except:
            break
    return sedata


def find_closest(val, arr):
    return np.argmin(np.abs(arr - val))


class RandomLearner(object):

    def __init__(self, nactions):
        self.nactions = nactions

    def best(self, *args, **kwargs):
        return random.choice(range(self.nactions))


class MCFastLearner(object):

    def __init__(self, mc_dynamics):
        
        self.position = 0.0
        self.velocity = 0.0
        self.mode_state = ModeState()

        self.learner = SarsaCmac(3, 0.3, .99, 0.9, 0.1, nlevels=2, resolution=0.1)
        self.save_directory = ""

        self.training_cnt = 0
        self.mc_dynamics = mc_dynamics
        
    def reset(self, is_random):
        return self.mc_dynamics.reset(random=is_random)

    def run_service(self, action, nsteps):
        return self.mc_dynamics.run_service(action, nsteps=nsteps)
    
    def get_mcstate(self):
        return self.mc_dynamics.get_mcstate()

    def is_finished(self):
        return self.position > 1.9

    def mesh(self, normalize=True):  # not a meshgrid
        if normalize is True:
            c = np.linspace(-1, 1, 20)
            return np.array([(i, j) for i in c for j in c])
        else:
            c = np.linspace(-2, 2, 20)
            d = np.linspace(-0.1, 0.1, 20)
            return np.array([(i, j) for i in c for j in d])

    def sample_value_function(self):
        vectors = self.mesh()
        values = self.learner.sample_values(vectors)
        return values  # put this in the diagnostic (and plot it)

    def observe(self, normalize=True):
        response = self.get_mcstate()
        result = self.state2array(response)
        if normalize is True:
            result[0] /= 2.0  # normalize position to [-1,1]
            result[1] /= 0.07  # normalize position to [-1,1]
        return result

    def finished(self):
        response = self.get_mcstate()
        return response[0] > 1.9

    def reward(self):
        response = self.get_mcstate()
        if response[0] > 1.9:
            return 10.0
        else:
            return -0.1

    def state2array(self, response):
        return np.array([response[0], response[1]])

    def train(self, nsteps=10, max_cnt=100000, verbose=False):
        training_actions = []
        goals_cnt = 0
        while self.training_cnt < max_cnt:
            self.training_cnt += 1

            if self.training_cnt % 1000 == 0:
                logger.info('Training count: {}'.format(self.training_cnt))
                cnts = Counter(training_actions)
                diagnostics = {'actions': cnts,
                               'nsteps': nsteps,
                               'goal_cnt': goals_cnt,
                               'diagnostics': self.learner.diagnostics(),
                               'vfunc': self.sample_value_function()}
                weblogger.info(json.dumps(diagnostics))  # , pickle.HIGHEST_PROTOCOL))

                training_actions = []
                goals_cnt = 0
                self.save()
                self.reset(is_random=True)

            if self.training_cnt % 10000 == 0 or self.training_cnt == 1:
                logger.info('Start test')
                avg_cnt = self.test(nsteps=nsteps, save_traces=True, observer=self.observe, learner=self.learner)
                logger.info('Average cnt: {}'.format(avg_cnt))
                continue # skip training

            pvector = self.observe()
            a = self.learner.softmax_policy(pvector)
            training_actions.append(a)
            self.run_service(action=a-1, nsteps=nsteps)
            vector = self.observe()
            action = a

            reward = self.reward()
            if self.finished():
                goals_cnt += 1
                self.reset(is_random=True)

            if verbose is True and self.training_cnt % 100 == 0:
                logger.info("{} {} {} {}".format(str(pvector), str(vector), action, reward))

            self.learner.train(pvector, action, reward, vector, self.learner.softmax_policy(vector))

    def test(self, nsteps=10, ntrials=10, save_traces=False):

        observer = self.observe
        learner = self.learner

        tests = []
        avg_cnt = 0
        for i in range(ntrials):
            if i % 10 == 0:
                logger.info('testing trial {}'.format(i))
            self.reset(is_random=True)
            cnt = 0
            test = []
            while not self.finished() and cnt < 1000:
                vector = observer()
                a = learner.best(vector)
                response = self.run_service(action=a-1, nsteps=nsteps)
                test.append((vector, a, self.state2array(response)))
                cnt += 1

            avg_cnt += cnt
            tests.append(test)

        if save_traces:
            fp = self.create_fp('test_traces')
            pickle.dump(tests, fp, pickle.HIGHEST_PROTOCOL)
            fp.close()

        result = float(avg_cnt) / float(ntrials)
        return result

    def set_save_directory(self, directory):
        self.save_directory = directory

    def create_fp(self,prefix):
        return create_fp(self.save_directory, prefix)

    def save(self):
        """
        Saves the state of the current leaner.
        """
        if self.save_directory:
            fp = self.create_fp('chk_pt_learner')
            pickle.dump(self.learner, fp, pickle.HIGHEST_PROTOCOL)
            fp.close()
        else:
            logger.info('Save directory not set! Not checkpointing...')

    def load(self, filename):
        fp = bz2.BZ2File(filename)
        self.learner = pickle.load(fp)
        fp.close()


class MCRandomLearner(MCFastLearner):

    def __init__(self, *args, **kwargs):
        super(MCRandomLearner, self).__init__(*args, **kwargs)
        self.learner = RandomLearner(3)

class MCFastLearnerSE(MCFastLearner):

    def __init__(self, *args, **kwargs):
        self.positions = deque(maxlen=2)
        super(MCFastLearnerSE, self).__init__(*args, **kwargs)
        self.learner = SarsaCmac(3, 0.2, 0.99, 0.8, 0.1, nlevels=2, resolution=0.1)

    def compute_velocity(self):

        if len(self.positions) == 2:
            x = self.positions.pop()
            y = self.positions.pop()
            vel = x - y
            self.positions.append(y)
            self.positions.append(x)

            if vel == 0:
                return self.previous_vel
            else:
                self.previous_vel = vel
                return vel
        else:
            self.previous_vel = 0.0
            return 0.0  # queue not full

    def observe(self, normalize=True):
        # return position using se state in (slow) version we'd compare the left_camera images - here we'll just
        # use the se state associated with the position (which comparing images would give us anyway)
        response = self.get_mcstate()
        [pos, vel] = self.state2array(response)
        se_pos = self.find_se_pos(pos)
        self.positions.append(se_pos)
        se_vel = self.compute_velocity()

        if normalize is True:
            se_pos = float(se_pos) / 10.0
            se_vel = float(se_vel)

        return np.array([se_pos, se_vel])

    def find_se_pos(self, pos):
        keys = self.se.keys()
        marg = find_closest(pos, np.array(keys))
        se_pos = self.se[keys[marg]]
        return se_pos

    def load_se_data(self, filename):
        """
        This file contains data of the following type:
        pickle.dump((i, position, Y[i, 0], pdata[0]), outfp, pickle.HIGHEST_PROTOCOL)

        Basically this is the output of sensorimotor embedding.
        """
        self.se = load_se(filename)
        self.se = {se[1]: se[2] for se in self.se}
        self.previous_se = -1

@click.command()
@click.option('--learn', is_flag=True, help='Learn a policy.')
@click.option('--test', is_flag=True, help='Test learning.')
@click.option('--directory', type=click.Path(exists=True, readable=True, file_okay=False), help='Directory of checkpoint files.')
@click.option('--mode', type=click.Choice(['se', 'random', 'normal']), help='What type of features/learner to use.')
@click.option('--se-data', type=click.Path(exists=True, readable=True, dir_okay=False), help="The file with sensorimotor embedding data.")
@click.option('--nsteps', default=1, help='The length of the action.')
@click.option('--save', type=click.Path(exists=True, readable=True, dir_okay=False), help="The path to a saved agent.")
@click.option('--ros', is_flag=True, help='Connect to ros. Sends commands to simulator.')
def cli(learn, test, directory, mode, se_data, nsteps, save, ros):

    if ros:
        mc = RosMountainCarController()
    else:
        mc = MountainCarController()

    se_mode = mode == 'se'
    random_mode = mode == 'random'
    normal_mode = mode == 'normal'

    if se_mode:
        logger.info('Running SE agent.')
        learner = MCFastLearnerSE(mc)
        learner.load_se_data(se_data)
    elif random_mode:
        logger.info('Running random agent.')
        learner = MCRandomLearner(mc)
    elif normal_mode:
        logger.info('Running regular agent.')
        learner = MCFastLearner(mc)
    else:
        raise ValueError, "Unknown mode!"

    if directory:
        learner.set_save_directory(directory)

    if learn:
        learner.train(nsteps=nsteps)

    if test:
        if save:
            learner.load(save)

        result = learner.test(nsteps)
        logger.info("RESULT: {}".format(result))

if __name__ == '__main__':
    cli()