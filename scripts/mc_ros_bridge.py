__author__ = 'Jeremy Stober <stober@gmail.com>'

import threading
import rospy
from gazebo_ros_vizmc.msg import MCState
from gazebo_ros_vizmc.srv import MCSetAction, MCReset, MCRunService, MCStateService
from mc_dynamics import MountainCarController

class RosMountainCarController(object):

    def __init__(self):
        self.set_action = rospy.ServiceProxy('/vizmc/set_action', MCSetAction)
        self.reset = rospy.ServiceProxy('/vizmc/reset', MCReset)
        self.run_service = rospy.ServiceProxy('/vizmc/run_service', MCRunService)
        self.get_mcstate = rospy.ServiceProxy('/vizmc/get_mcstate', MCStateService)
        self.toggle_mc = rospy.ServiceProxy('/vizmc/toggle_mc')