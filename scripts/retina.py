#!/usr/bin/python
"""
Author: Jeremy M. Stober
Program: RETINA.PY
Date: Thursday, May 15 2008
Description: A retina object that simulates various models of foveated vision.

A retinal model is a receptive field model combined with an
arrangement of these fields. The interfaces Field and FieldContainer
define the minimal interface needed by the Retina class.

Example usage of this module:

import retina
import camera

...

fr = retina.Retina(BoxLayers(range(1,33),32,1024))
cam = camera.Camera(image)
roi = cam.snap()
fr.project(img)
states = fr.states()
"""

from pylab import *
import numpy as np
import random as prandom
import numpy.random as npr
import threading

class Field(object):
    """
    Generic subclass that defines the interface for receptive fields
    in the retina model.
    """

    def __init__(self, *args, **kwargs):
        raise AttributeError, "Field class must be subclassed."

    def project(self, img):
        raise AttributeError, "Override method in subclass."

class FieldContainer(object):
    """
    Always empty container supporting the iterator protocol. Subclass
    this object to populate it with Fields.
    """

    def __init__(self, *args, **kwargs):
        raise AttributeError, "FieldContainer class must be subclassed."

    def __iter__(self):
        return self

    def next(self):
        raise StopIteration


# Some action initialization functions.
def azeros(x,y,idim,extent):
    return np.zeros(2)

def arandom(x,y,idim,extent):
    return npr.randint(-idim / 2, idim / 2, size=(2,))

def aoptimal(x,y,idim,extent):
        # The following is roughly the "optimal" policy for testing
        # purposes. Note that the optimal action moves the Retina such
        # that the ROI is centered at the fovea. Geometrically, this
        # means that the current active field moves away from the ROI
        # along the ray eminating from the fovea center and passing
        # through that pixel.
    return np.array([ x + extent / 2 - idim / 2 , y + extent / 2 - idim / 2 ])

class BoxField(Field):
    """
    We could consider fields of different types. This is a simple box
    shaped receptive field with a threshold function for determining
    the state from the average activation of the field.
    """

    def __init__(self, x, y, extent, idim, layer, threshold = 0.5, iaction = azeros, ireward = 0.0, alpha = 0.9, beta = 0.1):
        """
        Initialize a simple box model of a receptive field. x and y
        are the coordinates of the upper left hand corner of the
        field. extent specifies the field size. idim specifies the
        dimensions of the retina. threshold determines
        the state as a function of the average activation. The
        maxaction determines the limits of retina.
        """
        self.x = x
        self.y = y
        self.extent = extent
        self.idim = idim
        self.threshold = threshold
        self.size = float(extent**2)
        self.oldstate = -1
        self.state = 0
        self.alpha = alpha
        self.beta = beta

        
        self.reward = npr.randint(-15, 0)
        self.origreward = self.reward
        self.action = iaction(x, y, idim, extent)
        self.layer = layer # useful for display convenience

    def reset(self):
        self.reward = self.origreward

    def getReward(self):
        return self.state

    def project(self, img):
        """
        Needs to map the image to a state (0,1). img should be a numpy
        object. Treat x like a graphical component (as opposed to row).
        """
        (xmin,xmax) = (self.x,self.x+self.extent)
        (ymin,ymax) = (self.y,self.y+self.extent)

        assert len(img.shape) == 2

        stimulation = sum(img[ymin:ymax, xmin:xmax]) ;
        self.oldstate = self.state
        self.state = self.activation(stimulation) # 0-1

    def computeError(self):
        xAction = self.action[0]
        yAction = self.action[1]
        x = self.x + (self.extent / 2)
        y = self.y + (self.extent / 2)
        xError = (x - xAction) - self.idim/2
        yError = (y - yAction) - self.idim/2
        distError = math.sqrt(xError*xError + yError*yError)
        return distError


class PerfectField(BoxField):

    def __init__(self, *args, **kwargs):
        BoxField.__init__(self, *args, action = aoptimal, **kwargs)
        
class BinaryActivation(object):

    def activation(self,stimulation):
        """
        A threshold activation method.
        """
        return int((stimulation / self.size) > self.threshold)

class LinearActivation(object):

    def activation(self,stimulation):
        """
        A linear activation function.
        """
        return stimulation / self.size

class SimpleTrain(object):

    def train(self, reward, action, state = None):
        """
        Update only when observed reward is greater than estimated reward.
        """

        if state == None:
            oldstate = self.oldstate
        else:
            oldstate = state

        difference = reward - self.reward
        if difference > 0:
            # The action represents an improvement over the current estimate.
            self.reward += oldstate * self.alpha * difference
            self.action += oldstate * self.alpha * (action - self.action)

class RobustTrain(object):
    def train(self, reward, action, state = None):
        """
        Let reward track and only update policy when observed reward is greater than estimated reward.
        """

        if state == None:
            oldstate = self.oldstate
        else:
            oldstate = state

        difference = reward - self.reward
        if difference > 0:
            # The action represents an improvement over the current estimate.
            self.reward += oldstate * self.alpha * difference
            self.action += oldstate * self.alpha * (action - self.action)
        if difference < 0:
            # The action represents an deterioration over the current estimate. (Degeneration case.)
            self.reward += oldstate * self.beta * difference

class ComplexTrain(object):
    def train(self, reward, action, state=None, delta = 1.0):

        if state == None:
            oldstate = self.oldstate
        else:
            oldstate = state

        difference = reward - self.reward
        if difference > 0:
            # The action represents an improvement over the current estimate.
            self.reward += oldstate * self.alpha * difference
            self.action += oldstate * self.alpha * (action - self.action)
        if difference < 0:
            # if we are exploiting, we want low reward encounters to lower our estimate (delta == 1.0)
            # if we are exploring, we do not want low reward encounters to lower our estimate (delta == 0.0)
            self.reward += oldstate * self.beta * delta * difference
            

class DisableTrain(object):

    def train(self,reward,action,state=None):
        pass


"""
Construct receptive field by specifying the super-classes that handle
activation, learning rule, and field geometry.
"""

class SimplePerfectField(LinearActivation, SimpleTrain, PerfectField):
    pass

class SimpleLinearField(LinearActivation, SimpleTrain, BoxField):
    pass

class SimpleBinaryField(BinaryActivation, SimpleTrain, BoxField):
    pass

class RobustLinearField(LinearActivation, RobustTrain, BoxField):
    pass

class ComplexLinearField(LinearActivation, ComplexTrain, BoxField):
    pass

class BoxLayers(FieldContainer):
    """
    An iterator object that initializes and manages layers of BoxField
    objects. This model of the retina has overlapping fields of
    averaging boxes. To initialize different retinal models, first
    subclass Field with the desired receptive field model, then
    subclass FieldContainer with the proper initialization.
    """

    def __init__(self, extents, fdimension, idimension, rf=BoxField, **kw):
        """
        For now we assume a square ROI and square receptive fields.
        """

        assert extents[-1] <= idimension / fdimension # Otherwise big layers won't fit.

        self.nlayers = len(extents) # Number of layers.
        self.extents = extents # A list of extents - default (1,2,3,4,...).
        self.fdimension = fdimension # Receptive field dimension. (e.g. Number of receptive fields.)
        self.idimension = idimension # ROI dimension if using the fake camera.

        self.layers = []

        for (layer,extent) in enumerate(extents):
            start = idimension / 2 - (fdimension * extent) / 2 # Start the layer in the ROI.
            end = start + (fdimension * extent)
            positions = range(start,end,extent)
            self.layers.append([rf(i,j,extent,idimension,layer,**kw) for i in positions for j in positions])

    def __iter__(self):
        """
        Using a generator in an __iter__ method automatically returns
        an iterator (generator) object. There is no need to implement
        (or override) next().
        """

        for layer in self.layers:
            for field in layer:
                yield field

    def layer(self, num):
        return self.layers[num] # this is also an iterable

# Note locks cause problems with pickling.
#class Retina(synchronize.Synchronize):
class Retina(object):
    """
    A simulated foveated retina object. This class provides a
    container for receptive fields and manages projection of the image
    onto the active fields, and the reading of state and actions off
    of the field components.
    """

    def __init__(self, fields):
        """
        Initialize a retina object by passing in an array of receptive
        fields initialized in some way.
        """
        
        # create lock for synchronization
        #super(Retina,self).__init__(dsync=['normalize'])
        
        self.fields = fields # has project, state, action
        self.nlayers = fields.nlayers
        self.__lesion = None
        
        self.unscrambled = []
        for i in range(self.fields.nlayers):
            self.unscrambled.append(self.fields.layers[i][:])

        #self.maxvar = 0
        self.maxreward = 1.0
        self.alpha = 0.001
        self.estreward = 0.0
        # we are going to store all estimated rewards but we'd only need to store a fixed number to compute the changes needed
        self.estrewards = []
        self.tcounter = 0
    
    def vectors(self, num, scale = 1.0):
        """
        Returns the vectors for a single layer. These vectors
        represent the learned policies for each receptive field.
        """

        X = []
        Y = []
        U = []
        V = []
        for field in self.fields.layer(num):
            x = field.x + field.extent / 2
            y = field.y + field.extent / 2
            X.append( x * scale )
            Y.append( y * scale )
            U.append( -field.action[0] )
            V.append( -field.action[1] )
        return (X,Y,U,V)

    @staticmethod
    def normalize(img):
        """
        We should decide whether to encode 255 here or do something
        fancy (e.g. skip if the entire image is zero).
        """
        if np.max(img) == 0:
            return img
        else:
            return img / float(np.max(img))

    
    def project(self, img, normalize = False):
        """
        Project the image (which could correspond to the ROI) onto the
        retina by projecting it onto the receptive fields.
        """

        if normalize:
            img = self.normalize(img)

        if self.__lesion:
            partial = copy(img) # Not sure if img is a copy already or not.
            (xmin,xmax) = (self.__lesion[0],self.__lesion[2])
            (ymin,ymax) = (self.__lesion[1],self.__lesion[3])
            partial[ymin:ymax, xmin:xmax] = 0
            for field in self.fields:
                field.project(partial)
        else:
            for field in self.fields:
                field.project(img) # should modify state and action for each receptive field

    
    def reward(self, layer=None):
        if layer is None:
            return sum(array([field.getReward() for field in self.fields]))
        else:
            return array([field.getReward() for field in self.fields.layer(layer)])
    
    
    
    def states(self, layer=None):
        """
        Return a list of states corresponding to the results of image
        (ROI) projection onto the receptive fields.
        """
        if layer is None:
            return array([field.state for field in self.fields])
        else:
            return array([field.state for field in self.fields.layer(layer)])
    
    
        
    def positions(self, layer = None):
        if layer == None:
            return array([[field.x + (field.extent / 2), field.y + (field.extent / 2)] for field in self.fields])
        else:
            return array([[field.x + (field.extent / 2), field.y + (field.extent / 2)] for field in self.fields.layer(layer)])


        
    def rewards(self, layer=None):
        if layer == None:
            return array([field.reward for field in self.fields])
        else:
            return array([field.reward for field in self.fields.layer(layer)])

        
    def actions(self, layer=None):
        """
        Return a list of actions corresponding the the chosen action
        for each receptive field.
        """
        if layer == None:
            return array([field.action for field in self.fields])
        else:
            return array([field.action for field in self.fields.layer(layer)])

    
    def bound(self,action):

        baction = [0,0]
        idim = self.fields.idimension

        if action[0] > self.fields.idimension:
            baction[0] = self.fields.idimension
        elif action[0] < -self.fields.idimension:
            baction[0] = -self.fields.idimension
        else:
            baction[0] = action[0]

        if action[1] > self.fields.idimension:
            baction[1] = self.fields.idimension
        elif action[1] < -self.fields.idimension:
            baction[1] = -self.fields.idimension
        else:
            baction[1] = action[1]

        return array(baction)

    
    def raction(self):
        idim = self.fields.idimension
        return array([uniform(idim)-idim/2, uniform(idim)-idim/2])

    
    def action(self, variance=0.0):
        """
        Return the retina action based on the votes of the retina fields.
        """
        actions = self.actions()
        states = self.states()

        reward = self.meanReward() # estimate of what the reward should be
        # self.maxreward is a retina level moving average estimate of the max reward
                
        p = 0.0
        if self.maxreward > 0.0 and reward > 0.0:
            p = reward / self.maxreward # [0,1]
        
        action = None
        # Return the weighted average.
        if sum(states) == 0:
            action = actions.mean(axis=0) # Just return the average of all actions.
        else:
            action = dot(states,actions) / sum(states)

        raction = randn(2) * variance

        return self.bound( p * action + (1 - p) * raction ), p

        #return self.bound(action + (randn(2) * variance))

    
    def meanAction(self):
        """
        Return the mean of the actions for each receptive field that is in
        state=1
        """
        actions = self.actions()
        states = self.states()
        # Return the weighted average.
        if sum(states) == 0:
            return actions.mean(axis=0) # Just return the average of all actions.
        else:
            return dot(states,actions) / sum(states)

    
    def meanPosition(self):
        states = self.states()
        positions = self.positions()
        if sum(states) == 0:
            return positions.mean(axis = 0)
        else:
            return dot(states, positions) / sum(states)

    def rewardEstimate(self):
        rewards = self.rewards()
        return mean(rewards),var(rewards)
    
    def meanReward(self):
        states = self.states()
        rewards = self.rewards()
        if sum(states) == 0:
            return 0.0
        else:
            return dot(states, rewards) / sum(states)

    
    def lesion(self, rect):
        """
        The argument 'rect' [xmin ymin xmax ymax] specifies a region
        of the img to zero out prior to projection onto the retina.
        """
        self.__lesion = rect
        for field in self.fields:
            field.state = 0
            field.oldstate = 0
    
    def reset(self):
        print "RESETING!!"
        del self.pestreward
        self.tcounter = 0
        self.estreward = 0.0
        for field in self.fields:
            field.reset()
    
    def train(self, reward, action, aglobal=None, state=None, delta=1.0):
        
        if reward > self.maxreward:
            self.maxreward = reward
        else:
            # decay slowly
            pass
            # self.maxreward = self.maxreward - 0.001 * (self.maxreward - reward) 
        
        # track the recency weighted average (moving average) reward
        self.estreward += self.alpha * (reward - self.estreward)
        self.estrewards.append(self.estreward)
        
        if self.tcounter % 1000 == 0:
            if hasattr(self,'pestreward'):
                if self.estreward - self.pestreward < -10:
                    print "Reset: ", self.pestreward, self.estreward
                    self.reset()
                else:
                    print "Setting: ", self.pestreward, self.estreward
                    self.pestreward = self.estreward
                    
                
            else:
                self.pestreward = self.estreward
            
        if aglobal:
            for field in self.fields:
                field.train(reward,action,aglobal,delta=delta)
        elif state != None:
            for field, s in zip(self.fields,state):
                field.train(reward,action,s,delta=delta)
        else:
            for field in self.fields:
                field.train(reward,action,delta=delta)
    
        self.tcounter += 1
    
    def meanErrors(self):
        errors = asarray([field.computeError() for field in self.fields])
        return [errors.mean(axis=0), errors.std(axis=0)]
    
    def unscramble(self):
        for i in range(self.fields.nlayers):
            self.fields.layers[i] = self.unscrambled[i][:]

    
    def scramble(self):
        for i in range(self.fields.nlayers):
            prandom.shuffle(self.fields.layers[i])

    
    def expand(self,layer):
        """
        Expands a square array by duplicating entries by extent then zero
        padding so that it has dimension idim.
        """

        # First expand the state matrix according the the extent covered
        # by each receptive field. We can do this by taking the Kronecker
        # product of the state matrix A and B, a matrix of all ones with
        # dimension equal to the extent.

        states = array(self.states(layer))
        extent = self.fields.extents[layer]
        idim = self.fields.idimension
        fdim = self.fields.fdimension

        states = reshape(states,(fdim,fdim))
        S = kron(states, ones((extent,extent)))

        # Next we need to imbed S in the center of zero padded array of dimension idim
        I = zeros((idim,idim))
        indx = (idim - len(S)) / 2

        if indx > 0:
            I[indx:-indx,indx:-indx] = S
        else: # indx == 0 is a special case where this kind of indexing doesn't work
            I = S

        return I

def enn_mean(current, pts,e):
    neighbors = []
    for pt in pts:
        dist = sqrt(sum((current - pt.action)**2))
        if  dist < e:
            neighbors.append(pt)

    return mean([neighbor.action for neighbor in neighbors],0)

def gaussian(x,variance):
    return exp( -x / (2.0 * variance)) / (sqrt(2.0 * pi * variance))

def enn_gauss(current, pts):
    neighbors = []
    dsum = 0.0
    for pt in pts:
        dist = gaussian(sum((current - pt.action)**2), 20.0)
        neighbors.append((dist,pt))
        dsum += dist

    #pdb.set_trace()

    action = array([0.0,0.0])
    for weight,pt in neighbors:
        action += weight / dsum * pt.action

    return action


def smooth_gauss(retina):

    fields = [field for field in retina.fields]

#     enn_gauss(fields[0].action, fields)
    for field in fields:
        field.action = enn_gauss(field.action, fields)


def smooth_mean(retina):

    fields = [field for field in retina.fields]
    #actions = [field.action for field in retina.fields]
    for field in fields:
        field.action = enn_mean(field.action, fields, 10.0) # should modify the underlying retina


    #pickle.dump(retina,open('retina_mean.pck','w'))

def __test():
    """
    >>> img = zeros((1024,1024))
    >>> img[400:500,400:500] = 1.0  # In fovea activation.
    >>> retina = Retina(BoxLayers(range(1,33),32,1024,rf=SimpleBinaryField))
    >>> retina.project(img)
    >>> sum(retina.states())
    1989
    """

    import doctest
    doctest.testmod()


if __name__ == "__main__":

    __test()
