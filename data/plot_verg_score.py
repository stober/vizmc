__author__ = 'Jeremy Stober <stober@gmail.com>'
import cPickle as pickle
import numpy as np
import pylab

x = []
fp = open('/Users/stober/wrk/gazebo_ros_vizmc/data/testfile_20141009_00_10_1412830800.pck')
while True:
    try:
        x.append(pickle.load(fp))
    except:
        break

if False:
    y = np.array([(i['score'], i['gamma']) for i in x[:60]])

    pylab.plot(y[:,1], y[:,0], lw=2)
    pylab.title('Vergence Perceptual Goal')
    pylab.ylabel('Matching Score')
    pylab.xlabel('Vergence Angle')
    pylab.savefig('vergence_goal.png')

if True:
    sep = np.zeros((480, 10))
    y = [(i['left_image'], i['right_image'], i['score']) for i in x]
    combined = [np.hstack([i[0], sep, i[1]])  for i in y]
    for i in range(len(combined)):
        pylab.clf()
        pylab.imshow(combined[i], cmap='gray')
        pylab.title('Vergence Demo')
        pylab.text(0,-10, "Score: {:0.2f}".format(y[i][2]),  bbox=dict(facecolor='red', alpha=0.1))
        pylab.savefig('vizmc_verg_{}.png'.format(i))