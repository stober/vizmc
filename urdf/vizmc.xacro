<?xml version="1.0" ?>

<robot name="trackbot" xmlns:xacro="http://www.ros.org/wiki/xacro">

  <xacro:property name="baseline" value="1.0" /> <!-- Also used 1.4 -->

  <!-- Import all Gazebo-customization elements, including Gazebo colors -->
  <xacro:include filename="$(find gazebo_ros_vizmc)/urdf/vizmc.gazebo" />

  <!-- Import Rviz colors -->
  <xacro:include filename="$(find gazebo_ros_vizmc)/urdf/materials.xacro" />


  <xacro:macro name="default_inertial">
    <inertial>
      <mass value="10" />
      <inertia ixx="1.0" ixy="0.0" ixz="0.0"
	       iyy="1.0" iyz="0.0"
	       izz="1.0" />
    </inertial>
  </xacro:macro>

  <xacro:macro name="visual_block" params="color rotation position *shape">
    <visual>
      <origin rpy="${rotation}" xyz="${position}"/>
      <geometry>
	<xacro:insert_block name="shape" />
      </geometry>
      <material name="${color}" />
    </visual>
  </xacro:macro>

  <xacro:macro name="collision_block" params="rotation position *shape">
    <collision>
      <origin rpy="${rotation}" xyz="${position}" />
      <geometry>
	<xacro:insert_block name="shape" />
      </geometry>
    </collision>
  </xacro:macro>

  <xacro:macro name="transmission_macro" params="joint">
    <transmission name="${joint}_transmission">
      <type>transmission_interface/SimpleTransmission</type>
      <joint name="${joint}_joint"/>
      <actuator name="${joint}_motor">
	<hardwareInterface>EffortJointInterface</hardwareInterface>
	<mechanicalReduction>1</mechanicalReduction>
      </actuator>
    </transmission>    
  </xacro:macro>


  <link name="world"/>
  <joint name="base_joint" type="fixed">
    <parent link="world"/>
    <child link="base_link"/>
  </joint>

  <link name="base_link"/>

  <joint name="fixed_track" type="fixed">
    <parent link="base_link"/>
    <child link="track"/>
  </joint>

  <link name="track">
    <xacro:visual_block color="orange" rotation="0 0 0" position="0 0 0">
      <box size="4.0 0.2 0.2" />
    </xacro:visual_block>

    <xacro:collision_block rotation="0 0 0" position="0 0 0"> 
      <box size="4.0 0.2 0.2" />
    </xacro:collision_block>

    <xacro:default_inertial />
  </link>

  <joint name="track_joint" type="prismatic">
    <axis xyz="1 0 0" />
    <limit lower="-2.0" upper="2.0" effort="100" velocity="100"/>
    <parent link="track"/>
    <origin rpy="0 0 0" xyz="0 0 .2"/>
    <child link="slider"/>
  </joint>

  <link name="slider">
    <xacro:visual_block color="blue" rotation="0 0 0" position="0 0 0">
      <box size="0.2 0.2 0.2"/>
    </xacro:visual_block>

    <xacro:collision_block rotation="0 0 0" position="0 0 0">
        <box size="0.2 0.2 0.2"/>
    </xacro:collision_block>

    <xacro:default_inertial />
  </link>

  <joint name="mount_joint" type="revolute">
    <parent link="slider"/>
    <limit lower="-0.8" upper="0.8" effort="100" velocity="100" />
    <axis xyz="0 1 0"/>
    <origin rpy="0 0 0" xyz="0 0 .2"/>
    <child link="mount"/>
  </joint>


  <link name="mount">
    <xacro:visual_block color="orange" rotation="1.57 0 0" position="0 0 0">
	<cylinder length="${baseline}" radius="0.1"/>
    </xacro:visual_block>

    <xacro:collision_block rotation="1.57 0 0" position="0 0 0">
	<cylinder length="${baseline}" radius="0.1"/>
    </xacro:collision_block>

    <xacro:default_inertial/>
  </link>

  <joint name="left_mount_joint" type="revolute">
    <parent link="mount"/>
    <limit lower="-1.5" upper="1.5" effort="100" velocity="100"/>
    <origin rpy="0 0 0" xyz="0 ${baseline / 2 + 0.1} 0"/>
    <child link="left_mount"/>
    <axis xyz="0 0 1"/>
  </joint>

  <link name="left_mount">
    <xacro:default_inertial/>

    <xacro:visual_block color="blue" rotation="0 0 0" position="0 0 0">
      <cylinder length="0.2" radius="0.1" />
    </xacro:visual_block>

    <xacro:collision_block rotation="0 0 0" position="0 0 0">
      <cylinder length="0.2" radius="0.1" />
    </xacro:collision_block>
  </link>

  <joint name="right_mount_joint" type="revolute">
    <parent link="mount"/>
    <limit lower="-1.5" upper="1.5" effort="100" velocity="100"/>
    <origin rpy="0 0 0" xyz="0 -${baseline / 2 + 0.1} 0"/>
    <child link="right_mount"/>
    <axis xyz="0 0 1"/>
  </joint>

  <link name="right_mount">
    <xacro:default_inertial/>

    <xacro:visual_block color="blue" rotation="0 0 0" position="0 0 0">
      <cylinder length="0.2" radius="0.1" />
    </xacro:visual_block>

    <xacro:collision_block rotation="0 0 0" position="0 0 0">
      <cylinder length="0.2" radius="0.1" />
    </xacro:collision_block>
  </link>

  <joint name="right_camera_joint" type="fixed">
    <parent link="right_mount" />
    <origin rpy="0 0 0" xyz="0 -0.2 0" />
    <child link="right_camera" />
  </joint>

  <link name="right_camera">
    <xacro:default_inertial />

    <xacro:visual_block color="orange" rotation="0 0 0" position="0 0 0">
      <box size="0.2 0.2 0.2" />
    </xacro:visual_block>

    <xacro:collision_block  rotation="0 0 0" position="0 0 0">
      <box size="0.2 0.2 0.2" />
    </xacro:collision_block>
 </link>

  <joint name="left_camera_joint" type="fixed">
    <parent link="left_mount" />
    <origin rpy="0 0 0" xyz="0 0.2 0" />
    <child link="left_camera" />
  </joint>

  <link name="left_camera">
    <xacro:default_inertial />

    <xacro:visual_block color="orange" rotation="0 0 0" position="0 0 0">
      <box size="0.2 0.2 0.2" />
    </xacro:visual_block>

    <xacro:collision_block  rotation="0 0 0" position="0 0 0">
      <box size="0.2 0.2 0.2" />
    </xacro:collision_block>
 </link>

 <xacro:transmission_macro joint='track'/>
 <xacro:transmission_macro joint='mount'/>
 <xacro:transmission_macro joint='right_mount'/>
 <xacro:transmission_macro joint='left_mount'/>


</robot>
